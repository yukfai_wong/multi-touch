import sys
from pathlib import Path
import numpy as np
import cv2


def main(file):
    # Configure depth and color streams
    frames = np.load(file)
    color_frame= frames[:,:,:,:3].astype(np.uint8)
    depth_frame= frames[:,:,:,3].astype(np.uint16)
    Total_frame=frames.shape[0]
    frames=0
    print("Total_frame: "+str(Total_frame))
    for i in range(Total_frame):
        # Convert images to numpy arrays
        color_image = color_frame[i]
        depth_image = depth_frame[i]
        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

        # Stack both images horizontally
        images = np.hstack((color_image, depth_colormap))

        # Show images
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', images)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    # execute only if run as a script
    if(len(sys.argv)<2):
        print("enter path to file")
    elif(Path(sys.argv[1]).is_file()):
        main(sys.argv[1])
    else:
        print("File do not exist")
