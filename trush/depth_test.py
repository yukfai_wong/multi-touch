import pyrealsense2 as rs
import numpy as np
import cv2
import time
# Configure depth and color streams
config = rs.config()
print("config")
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
print("enable_depth_stream")
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
print("enable_color_stream")

pipeline = rs.pipeline()
print("pipeline")
# Start streaming
profile = pipeline.start(config)
print("start")
depth_sensor = profile.get_device().first_depth_sensor()
depth_unit = depth_sensor.get_option(rs.option.depth_units)
print(depth_unit)
try:
    while True:

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()
        if not depth_frame or not color_frame:
            continue

        width = depth_frame.get_width()
        height = depth_frame.get_height()
        dist_to_center = depth_frame.get_distance(640, 480);

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())
        cal_dist_to_center= depth_image[479, 639]*depth_unit
        print("dist_to_center: "+str(dist_to_center)+"\n cal_dist_to_center: "+str(cal_dist_to_center))
        print(depth_image[479, 639])
        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

        # Stack both images horizontally
        images = np.hstack((color_image, depth_colormap))

        # Show images
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', images)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        time.sleep(1)
except RuntimeError:
    print("No more frames arrived, reached end of BAG file!")

finally:

    # Stop streaming
    pipeline.stop()
