
https://bitbucket.org/tomwong1027/multi-touch/src/master/
#University of Glasgow  years4 project

Multitouch interaction from virtual surfaces (ID: 7239)
Owner/Supervisor: Antoine Loriette
*Suitable as a Software Engineering project.
Description:
The use of depth cameras to create virtual touch surfaces on regular tabletops affords flexibility and interaction solutions for situationally or physically impaired users. However, relying on optical tracking remains challenging in term of precision of the positional estimation and robustness of the touch classification.

In this project, we propose to rely on recent advances in machine learning to bring improvements to the task of touch classification. This involves creating an annotated dataset, testing existing techniques, developing and testing a CNN-based model.

supervision with Dr Sebastian Stein
Special Requirements :
None

module dependency
Python 3.5+
anaconda are required
opencv: https://github.com/opencv/opencv
realsense: https://github.com/IntelRealSense/librealsense
keras

for view produce:
cd multi-touch
python tool\realTime_both_Vhdf5.py recording_data\2_tap.hdf5

\notebook     it is the footstep of everything in jupyter notebook. Use jupyter to open it
\experiement     all the graph generation, data recording, labeling tools,
experiement\dissertation_experiment.ipynb graph generation that used in dessertation
experiement\Evaluation_tools.py           tool kit used to generate confusion metrix, recll, precision
experiement\labeling.py           labeling tool
experiement\labeling_recorrect.py       recorrect labelling



\tool         all the stable version of tool
tool\align2partten.py       homography align tool
tool\bag_2_hdf5.py          tranform .bag file to hdf5 file
tool\depth_angle_model.hdf5 keras touch classificater
tool\depth_carema.py        connect to camera view all the steam data
tool\HandShapeAnalysis.py   fingertips detection model
tool\read_from_file_Vhdf5.py read saved data from .hdf5
tool\read_from_file_Vpyrealsense2.py  read saved data from .bag
tool\realTime_birdeyes_Vhdf5.py real time model of fingertips detection (birdeyes flow) + touch classification
tool\realTime_both_camera.py real time model of combined flow fingertips detection(camera flow) + touch classification
tool\realTime_both_Vhdf5.py   offline model of combined flow fingertips detection(camera flow) + touch classification

tool\save_test_Vhdf5_homography.py      generate dataset in hdf5 foormat
tool\save_test_Vpyrealsense2.py      generate dataset in bag foormat
