from homography import pyrs12, spaces
import os, cv2, h5py, random, string
import numpy as np
import matplotlib.pyplot as plt
import deproject
import pptk
import tqdm

class Confusion(object):
    def __init__(self):
        self.TPL = []
        self.TNL = []
        self.FPL = []
        self.FNL = []
        self.detection = 0
        self.real_tn = 0

    @property
    def TP(self):
        return len(self.TPL)
    @property
    def TN(self):
        return len(self.TNL)
    @property
    def FP(self):
        return len(self.FPL)
    @property
    def FN(self):
        return len(self.FNL)

    @property
    def recall(self):
        if (self.TP+self.FN) == 0:
            return -1
        return self.TP/(self.TP+self.FN)

    @property
    def precision(self):
        if (self.TP+self.FP) == 0:
            return -1
        return self.TP/(self.TP+self.FP)

    @property
    def Sensitivity(self):
        if (self.TP+self.FN) == 0:
            return -1
        return self.TP/(self.TP+self.FN)

    @property
    def Specificity(self):
        if (self.TN+self.FP) == 0:
            return -1
        return self.TN/(self.TN+self.FP)

    def combine(self, neighbor):
        new = Confusion()
        new.detection = self.detection + neighbor.detection
        new.real_tn = self.real_tn + neighbor.real_tn
        new.TPL = [*self.TPL, *neighbor.TPL]
        new.TNL = [*self.TNL, *neighbor.TNL]
        new.FPL = [*self.FPL, *neighbor.FPL]
        new.FNL = [*self.FNL, *neighbor.FNL]
        return new

    def show(self):
        print("Total detection: {:d}".format(self.detection))
        print("real_tn        : {:d}".format(self.real_tn))
        print("True  Positive : {:d}".format(self.TP))## tips detected and is labeled
        print("False Positive : {:d}".format(self.FP))## tips detected and not labeled
        print("True  Negative : {:d}".format(self.TN))## tips not detected and not labeled (5-not show finger)
        print("False Negative : {:d}".format(self.FN))## tips not detected and is labeled
        print("recall : {:f}".format(self.recall))
        print("precision : {:f}".format(self.precision))
        print()



##yellow = all detected finger tips
##green all labeled

##red = all detected

##blue = mini missmatch label

def Confusion_Matrix(depth_frame, label, range_, analyser, function,debug = True, new_parameter=False,threathold =12):
    distance = []
    total = 0
    total_detectation = 0
    total_label = 0
    c = Confusion()
    fnum = [Confusion(),Confusion(),Confusion(),Confusion(),Confusion()]
    if new_parameter:
        analyser.angel=40
#         analyser.cloud_min=0.015
        analyser.combine_minimun_distance=0.03
        analyser.cluster_threshold_=30
    for i in range_:
        areas = function(depth_frame[i])
        label_i = label[i].tolist()
        impoint = []
        if(len(analyser.finger_point_cloud_coordinate)!=0):
            impoint = cloudp2imagep(analyser.cloud, analyser.finger_point_cloud_coordinate)
            if debug:
                print("frame: ",i)
            label_i = label[i].tolist()

            temp_image = np.copy(analyser.finger_point_image)
            plt.show()
            cv2.drawContours(temp_image, np.array(impoint).reshape(-1, 1, 2), -1, (255, 255, 0), 12)
            cv2.drawContours(temp_image, np.array(label[i]).reshape(-1, 1, 2), -1, (0, 255, 0), 8)
        else:
            print(i)
            print("can't detect any finger")
            plt.imshow(analyser.process_image)
            plt.show()

        if (len(label_i)==0) & debug:
            print("--------------------")
            print("{:d} did not label".format(i))
            print("--------------------")
            continue
        else:
            total_detectation+=len(impoint)
            total_label += len(label_i)
            finger_number = len(label_i)-1
            fnum[finger_number].real_tn += len(label_i)
            fnum[finger_number].detection +=len(impoint)
        while((len(label_i)>0) & (len(impoint)>0)):
            l = label_i.pop()
#             p = find_point(depth_frame[i], l)
            dis = np.linalg.norm(impoint - l,axis = 1)
            mini = np.argmin(dis)
            if debug:
                print("    distance: ", dis[mini])
            if (dis[mini]>threathold):
                c.FPL.append(len(label_i))
                fnum[finger_number].FPL.append(len(label_i))

                c.FNL.append(len(label_i))
                fnum[finger_number].FNL.append(len(label_i))
                reload = np.copy(analyser.finger_point_image)
                cv2.drawContours(reload, np.array(impoint[mini]).reshape(-1, 1, 2), -1, (255, 255, 0), 10)
                cv2.drawContours(reload, np.array(l).reshape(-1, 1, 2), -1, (0, 255, 0), 8)
                if debug:
                    print("     minimun distinct unmatch =")
                    fig = plt.figure(figsize = (12, 4))
                    ax1 = fig.add_subplot(1,2,1)
                    ax2 = fig.add_subplot(1,2,2)
                    ax1.imshow(reload, interpolation='nearest', aspect='auto')
                    ax2.imshow(temp_image, interpolation='nearest', aspect='auto')
                    plt.show()
                impoint = np.delete(impoint, np.s_[mini], axis=0)
            else:
                c.TPL.append(len(label_i))
                fnum[finger_number].TPL.append(len(label_i))
                distance.append(dis[mini])
                impoint = np.delete(impoint, np.s_[mini], axis=0)
        if(len(impoint)>0):
            for i in range(len(impoint)):
                c.FPL.append(len(impoint))
                fnum[finger_number].FPL.append(len(impoint))
            if debug:
                print("    remain(could not match any label)",len(impoint))
                cv2.drawContours(temp_image, np.array(impoint).reshape(-1, 1, 2), -1, (255, 0, 0), 6)
                plt.figure(figsize = (6, 4))
                plt.imshow(temp_image, interpolation='nearest', aspect='auto')
                plt.show()
        if(len(label_i)>0):
            for i in range(len(label_i)):
                c.FNL.append(len(label_i))
                fnum[finger_number].FNL.append(len(label_i))
            if debug:
                print("{:d} fingertips are not being detected".format(len(impoint)))
    c.detection = total_detectation
    c.real_tn = total_label
    return c, distance, fnum



def Confusion_Matrix_touch(analyser, model, depth_frame, label_label, label_istouch, range_, debug = True,threathold =12):
    total = 0
    total_detectation = 0
    total_label = 0
    tips_detection = Confusion()
    touch_detection = Confusion()
    distance = []

    evaluation_touch_area = []
    evaluation_touch_label = []
    fnum = [Confusion() for i in range(5)]#5 finger at max
    if not debug:
        range_ = tqdm.tqdm(range_)
    for i in range_:
        areas, points = analyser.renew_both(depth_frame[i])

        impoint = []
        label_label_i = label_label[i].tolist()
        label_istouch_i = label_istouch[i]
        if(len(analyser.finger_point_cloud_coordinate)!=0):
            impoint = cloudp2imagep(analyser.cloud, analyser.finger_point_cloud_coordinate)
            if debug:
                print("frame: ",i)
            temp_image = np.copy(analyser.finger_point_image)
            cv2.drawContours(temp_image, np.array(impoint).reshape(-1, 1, 2), -1, (255, 255, 0), 10)
            cv2.drawContours(temp_image, np.array(label_label_i).reshape(-1, 1, 2), -1, (0, 0, 255), 8)

        if (len(label_label_i)==0) & debug:
            print("-----------")
            print("{:d} did not label".format(i))
            print("-----------")
            continue
        else:
            total_detectation+=len(impoint)
            total_label += len(label_label_i)
            finger_number = len(label_label_i)-1
            fnum[finger_number].detection +=len(impoint)
            fnum[finger_number].real_tn += len(label_label_i)
        while((len(label_label_i)>0) & (len(impoint)>0)):
            ll = label_label_i.pop()
            if(len(label_istouch_i)!=0):
                lt = label_istouch_i.pop()
#             p = find_point(depth_frame[i], l)
            dis = np.linalg.norm(impoint - ll,axis = 1)
            mini = np.argmin(dis)
            if debug:
                print("    distance: ", dis[mini])
            if (dis[mini]>threathold):
                tips_detection.FPL.append(len(ll))
                fnum[finger_number].FPL.append(len(label_label_i))

                tips_detection.FNL.append(len(ll))
                fnum[finger_number].FNL.append(len(ll))
                reload = np.copy(analyser.finger_point_image)
                cv2.drawContours(temp_image, np.array(impoint[mini]).reshape(-1, 1, 2), -1, (255, 255, 0), 10)
                cv2.drawContours(temp_image, np.array(ll).reshape(-1, 1, 2), -1, (0, 0, 255), 8)

                evaluation_touch_area.append(np.array([areas[mini]]))
                evaluation_touch_label.append(False)

                if debug:
                    print("     minimun distinct unmatch =")
                    fig = plt.figure(figsize = (12, 4))
                    ax1 = fig.add_subplot(1,2,1)
                    ax2 = fig.add_subplot(1,2,2)
                    ax1.imshow(reload, interpolation='nearest', aspect='auto')
                    ax2.imshow(temp_image, interpolation='nearest', aspect='auto')
                    plt.show()
                impoint = np.delete(impoint, np.s_[mini], axis=0)
            else:
                tips_detection.TPL.append(len(ll))
                fnum[finger_number].TPL.append(len(label_label_i))
                distance.append(dis[mini])

                evaluation_touch_area.append(np.array([areas[mini]]))
                evaluation_touch_label.append(True)

                impoint = np.delete(impoint, np.s_[mini], axis=0)
                areas = np.delete(areas, np.s_[mini], axis=0)
            ll = None
            lt = None
        if(len(impoint)>0):
            for i in range(len(impoint)):
                tips_detection.FPL.append(len(impoint))
                fnum[finger_number].FPL.append(len(impoint))
                evaluation_touch_area.append(areas[i])
                evaluation_touch_label.append(False)
            if debug:
                print("    remain(could not match any label)",len(impoint))
                cv2.drawContours(temp_image, np.array(impoint).reshape(-1, 1, 2), -1, (255, 0, 0), 8)
                plt.figure(figsize = (6, 4))
                plt.imshow(temp_image, interpolation='nearest', aspect='auto')
                plt.show()
        if(len(label_label_i)>0):
            for i in range(len(label_label_i)):
                tips_detection.FNL.append(len(label_label_i))
                fnum[finger_number].FNL.append(len(label_label_i))
            if debug:
                print("{:d} fingertips are not being detected".format(len(impoint)))

    x_predicts = analyser.predict_return(evaluation_touch_area)
    y_predict = model.predict(x_predicts)
    for i in range(len(y_predict)):
        if(y_predict[i][0]==1):
            if(evaluation_touch_label[i]):
                touch_detection.TPL.append(1)
            else:
                touch_detection.FPL.append(1)
        else:
            if(evaluation_touch_label[i]):
                touch_detection.FNL.append(1)
            else:
                touch_detection.TNL.append(1)
    tips_detection.detection = total_detectation
    tips_detection.real_tn = total_label
    return tips_detection, touch_detection, fnum,distance



def cloudp2imagep(cloud,point):
    new = []
    for i in point:
        dist = np.linalg.norm(cloud - i,axis = 2)
        new.append(np.unravel_index(dist.argmin(), dist.shape)[::-1])#yx2xy
    return np.array(new)
