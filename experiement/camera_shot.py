from pathlib import Path
from homography import pyrs12, spaces
from termcolor import colored
import sys
import os
import time
import cv2
import h5py
import datetime
import numpy as np
import deproject
import HandShapeAnalysis
import argparse
import random
import copy

save_path = "./recording/"

def ex1(round):
    cam = pyrs12.Camera()
    cam.start()
    print("camera start")
    now=datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    os.makedirs(save_path+"ex1_"+now+"/")
    path = save_path+"ex1_"+now+"/Vhdf5_ex1_"+now+".hdf5"
    recording_file = h5py.File(path, 'a')
    recording_file.create_dataset(name="color", shape=(0,480,640,3), maxshape=(None,480,640,3), dtype=np.uint8, chunks=(1,480,640,3), compression="gzip", compression_opts=4)
    recording_file.create_dataset(name="depth", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint16, chunks=(1,480,640), compression="gzip", compression_opts=4)
    recording_file.create_dataset(name="IR", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint8, chunks=(1,480,640), compression="gzip", compression_opts=4)
    recording_file.create_dataset(name="fingers", shape=(0,1), maxshape=(None,1), dtype=np.int8, chunks=(10,1))
    record_partten(cam, recording_file)
    print("Save at ", path)

    input("Pre-process complete, Press Enter to start experiment documentation...")
    print("--------------------------------------------------------------")
    print("Welcome to the Project Multitouch interaction from virtual surfaces Experiment 2")
    print("You are required to show the exact number of finger(s) as the screen display. E.g.")
    print(colored("1 fingers", 'red'))
    print("Countdown(3,2,1,0) will then started and a screenshot will be taken afterward.")
    input("Press Enter to start experiment...")
    print("--------------------------------------------------------------")
    print("--------------------------------------------------------------")
    print("--------------------------------------------------------------")


    try:
        frame_num = -1
        kill = False
        for r in range(round):
            print("Round: %d"%r)
            for finger in range(5):
                print(colored("%d %s"%(finger+1, "finger" if (finger+1)==1 else "fingers"), 'red'))
                countdown()
                frames = cam.wait_for_frames()
                print("Captured")
                color_image, depth_image, IR_image = cam.cdi

                frame_num+=1
                new_shape = frame_num +1
                recording_file['color'].resize(new_shape, axis=0)
                recording_file['depth'].resize(new_shape, axis=0)
                recording_file['IR'].resize(new_shape, axis=0)
                recording_file['color'][frame_num]=color_image
                recording_file['depth'][frame_num]=depth_image
                recording_file['IR'][frame_num]=IR_image

                # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
                depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
                draw_gray_image = cv2.cvtColor(IR_image, cv2.COLOR_GRAY2BGR)

                # Stack both images horizontally
                images = np.hstack((color_image, depth_colormap, draw_gray_image))
                images = cv2.resize(images, (960, 240))

                # Show images
                cv2.imshow('RealSense', images)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    kill = True
                    recording_file.close()
                    break
            if(kill):
                break
        print("End")
    finally:
        # Stop streaming
        recording_file.close()
        cam.stop()

def ex2(round):
    cam = pyrs12.Camera()
    cam.start()
    print("camera start")
    now=datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    os.makedirs(save_path+"ex2_"+now+"/")
    path = save_path+"ex2_"+now+"/Vhdf5_ex2_"+now+".hdf5"
    recording_file = h5py.File(path, 'a')
    recording_file.create_dataset(name="color", shape=(0,480,640,3), maxshape=(None,480,640,3), dtype=np.uint8, chunks=(1,480,640,3), compression="gzip", compression_opts=4)
    recording_file.create_dataset(name="depth", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint16, chunks=(1,480,640), compression="gzip", compression_opts=4)
    recording_file.create_dataset(name="IR", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint8, chunks=(1,480,640), compression="gzip", compression_opts=4)
    record_partten(cam, recording_file)
    print("Save at ", path)
    input("Pre-process complete, Press Enter to start experiment...")
    print("Welcome to the Project Multitouch interaction from virtual surfaces Experiment 2")
    print("Please remember the following indexing of finger")
    print("""              3                |
          2  /"\  4            |
         /"\|\./|/"\           |
        |\./|   |\./|          |
        |   |   |   | 5        |
        |   |   |   |/"\       |
        |   |   |   |\./|      |
      1 |   |   |   |   |      |
     /~\|   |   |   |   |      |
    |_/ |   |   |   |   |      |
    |   |           |   |      |
    |   |               |      |
    |   '               |      |
    \                   |      |
     \                 /       |
      \               /        |
       \.            /         |
         |          |          |
         |          |          |
    i.e. thumb:0 index finger:1 middle finger:2 ring finger:3 pinky:4
    """)
    print("The screen display which finger(s) you will need to show and ")
    print("which finger(s) you are required to touch the table")
    print("After you confident of your post :D, click Enter with other hand and a screenshot will be captured.")
    print("Here is an example:")
    ex2_print()
    print("let's start.")
    print("--------------------------------------------------------------")

    try:
        frame_num = -1
        kill = False
        show_touch_tuple = []
        for r in range(round):
            print("Round: %d"%r)
            for finger in range(5):
                st = ex2_print()
                show_touch_tuple.append(st)
                input("confident?")
                frames = cam.wait_for_frames()
                print("Captured")
                color_image, depth_image, IR_image = cam.cdi

                frame_num+=1
                new_shape = frame_num +1
                recording_file['color'].resize(new_shape, axis=0)
                recording_file['depth'].resize(new_shape, axis=0)
                recording_file['IR'].resize(new_shape, axis=0)
                recording_file['color'][frame_num]=color_image
                recording_file['depth'][frame_num]=depth_image
                recording_file['IR'][frame_num]=IR_image

                # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
                depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
                draw_gray_image = cv2.cvtColor(IR_image, cv2.COLOR_GRAY2BGR)

                # Stack both images horizontally
                images = np.hstack((color_image, depth_colormap, draw_gray_image))
                images = cv2.resize(images, (960, 240))

                # Show images
                cv2.imshow('RealSense', images)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    kill = True
                    recording_file.close()
                    break
            if(kill):
                break
        print("End")
    finally:
        # Stop streaming
        print(np.array(show_touch_tuple))
        np.save(save_path+"ex2_"+now+"/show_touch_tuple", np.array(show_touch_tuple))

        recording_file.close()
        cam.stop()

def record_partten(cam, recording_file):
    try:
        print("Wasting first 30 loop, please don't move the camera")
        for i in range(30):
            cam.wait_for_frames()
            print(i,". ", end="", flush=True)
        print()
        pcl_center, trans_matrix = get_partten(cam)
        while np.isnan(trans_matrix[0][0]):
            print("Error: partten ")
            pcl_center, trans_matrix = get_partten(cam)
        print("partten recorded")
        print("pcl_center")
        print(pcl_center)
        print("trans_matrix")
        print(trans_matrix)
    except:
        print("Something happen while recording partten, Stop")
        cam.stop()
        recording_file.close()
    else:
        recording_file.create_dataset(name="pcl_center", shape=(3,), dtype=np.float32, chunks=(3,), data = pcl_center)
        recording_file.create_dataset(name="trans_matrix", shape=(3,3), dtype=np.float32, chunks=(3,3), data = trans_matrix)


def ex2_print():
    show = [0,1,2,3,4]
    random.shuffle(show)
    show = sorted(show[:random.SystemRandom().randrange(1,len(show))])
    touch = copy.deepcopy(show)
    print("Please Show:")
    print(colored(list(map(lambda x: x+1 , show), 'red')))
    print(print_finger_name(show))
    if len(touch)>1:
        random.shuffle(touch)
        touch = sorted(touch[:random.SystemRandom().randrange(1,len(touch))])
    print("Please touch:")
    print(colored(list(map(lambda x: x+1 , touch), 'red')))
    print(print_finger_name(touch))
    return show, touch

def print_finger_name(l):
    name = ["thumb", "index finger", "middle finger", "ring finger", "pinky"]
    s=[]
    for i in l:
        s.append(name[i])
    return s


def countdown(s = 3,e = -1,step = -1):
    for i in range(s,e,step):
        time.sleep(1)
        print(i)

def get_partten(cam):
    scene = spaces.Scene(cam, './homography/rgb-marker.png')
    pcl_center = scene.system.pcl_center
    trans_matrix = scene.system.trans_matrix
    return pcl_center, trans_matrix

if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser()
    parser.add_argument("-ex1", help="Join experiement1",action="store_true")
    parser.add_argument("-ex2", help="Join experiement2",action="store_true")
    parser.add_argument("round", type = int, help="How many round to play")
    args = parser.parse_args()
    print(args.ex1,args.ex2)
    if(args.round>0):
        if(args.ex1):
            ex1(args.round)
        elif(args.ex2):
            ex2(args.round)
