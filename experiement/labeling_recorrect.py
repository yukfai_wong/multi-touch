import sys
from pathlib import Path

import os, cv2, h5py
import numpy as np
import HandShapeAnalysis


all_point = []
current_image_point = []
current_image = None

def click(event, x, y, flags, param):
    global point, current_image
    if event == cv2.EVENT_LBUTTONDOWN:
        current_image_point.append([x,y])
        print("current_image_point: ", current_image_point)
    elif event == cv2.EVENT_RBUTTONDOWN:
        min_idx = np.argmin(np.linalg.norm(np.array(current_image_point)-np.array([x,y]), axis=1))
        del current_image_point[min_idx]
        print("current_image_point: ", current_image_point)
    else:
        return
    new = np.copy(current_image)
    cv2.drawContours(new, np.array(current_image_point).reshape(-1, 1, 2), -1, (0, 0, 255), 6)
    cv2.imshow("image", new)

cv2.namedWindow('image', cv2.WINDOW_AUTOSIZE)
cv2.setMouseCallback("image", click)

h5f = h5py.File("./recording/ex2_20190131_171522/Vhdf5_ex2_20190131_171522.hdf5", 'r')
pcl_center = h5f['pcl_center'][...]
trans_matrix = h5f['trans_matrix'][...]
h5f.close()
def main(file):
    global current_image, current_image_point, all_point
    # Configure depth and color streams
    h5f = h5py.File(file, 'r')
    depth_frame = h5f['depth'][...]

    analyser2 = HandShapeAnalysis.Depth2Finger(pcl_center, trans_matrix)
    analyser2.set_show(debug=False,
                   finger_point_image=True, hull_image=True, detected_defect=True, cluster_point=True, possibility_modal=False)
    analyser2.set_cloud_min(0.02)
    analyser2.RANSAC = True

    Total_frame = depth_frame.shape[0]
    print("Total_frame: %d"%Total_frame)

    output = os.path.splitext(file)[0]
    label = np.load(output+".npy")

    finger_id = None
    try:
        finger_id = np.load(os.path.dirname(file)+"/show_touch_tuple.npy")
    except Exception:
        pass

    try:
        for i in range(Total_frame):
            try:
                print(finger_id[i])
            except Exception as e:
                print(e)

            depth_image = depth_frame[i]

            current_image_point = list(label[i])

            analyser2.renew_both(depth_image)
            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = np.where(analyser2.process_image>0,255,0).astype(np.uint8)
            depth_colormap = cv2.cvtColor(depth_colormap,cv2.COLOR_GRAY2RGB)
            current_image = np.copy(depth_colormap)
            cv2.drawContours(depth_colormap, np.array(label[i]).reshape(-1, 1, 2), -1, (0, 0, 255), 6)
            # Stack both images horizontally
            cv2.imshow('image', depth_colormap)
            # Show images
            key = cv2.waitKey(0) & 0xFF
            if key == ord("r"):
                all_point.append(np.array(current_image_point))
                print("save")
                current_image_point = []
                print("%d/%d"%(i+1,Total_frame))
            if key == ord("q"):
                raise quit("Someone press q.")
    except Exception as e:
        print(e)
    else:
        np.save(output, all_point)
    cv2.destroyAllWindows()
    h5f.close()

class quit(Exception):
    pass

if __name__ == "__main__":
    # execute only if run as a script
    if(len(sys.argv)<2):
        print("enter path to file")
    elif(Path(sys.argv[1]).is_file()):
        main(sys.argv[1])
    else:
        print("File do not exist")
