import sys
from pathlib import Path

from homography import pyrs12, spaces
import os, cv2, h5py
import numpy as np
import warnings

warnings.warn("Please label with the reverse x-axis order ")

all_point = []
current_image_point = []
current_image = None

def click(event, x, y, flags, param):
    global point, current_image
    if event == cv2.EVENT_LBUTTONDOWN:
        current_image_point.append([x,y])
        print("current_image_point: ", current_image_point)
    elif event == cv2.EVENT_RBUTTONDOWN:
        min_idx = np.argmin(np.linalg.norm(np.array(current_image_point)-np.array([x,y]), axis=1))
        del current_image_point[min_idx]
        print("current_image_point: ", current_image_point)
    else:
        return
    new = np.copy(current_image)
    cv2.drawContours(new, np.array(current_image_point).reshape(-1, 1, 2), -1, (0, 0, 255), 10)
    cv2.imshow("image", new)

cv2.namedWindow('image', cv2.WINDOW_AUTOSIZE)
cv2.namedWindow('color', cv2.WINDOW_AUTOSIZE)
cv2.setMouseCallback("image", click)

def main(file):
    global current_image, current_image_point, all_point
    # Configure depth and color streams
    h5f = h5py.File(file, 'r')
    color_frame= h5f['color']
    depth_frame= h5f['depth']
    IR_frame= h5f['IR']

    Total_frame = h5f['color'].shape[0]
    print("Total_frame: %d"%Total_frame)

    output = os.path.splitext(file)[0]

    finger_id = None
    try:
        finger_id = np.load(os.path.dirname(file)+"/show_touch_tuple.npy")
    except Exception:
        pass

    try:
        for i in range(Total_frame):
            if finger_id is not None:
                print(finger_id[i])
            depth_image = depth_frame[i]
            color_image = color_frame[i]
            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
            current_image = depth_colormap
            # Stack both images horizontally
            cv2.imshow('image', depth_colormap)
            cv2.imshow('color', color_image)
            # Show images
            key = cv2.waitKey(0) & 0xFF
            if key == ord("r"):
                all_point.append(np.array(current_image_point))
                print("save")
                current_image_point = []
                print("%d/%d"%(i+1,Total_frame))
            if key == ord("q"):
                raise quit("Someone press q.")
    except Exception as e:
        print(e)
    else:
        np.save(output, all_point)
    cv2.destroyAllWindows()
    h5f.close()

class quit(Exception):
    pass

if __name__ == "__main__":
    # execute only if run as a script
    if(len(sys.argv)<2):
        print("enter path to file")
    elif(Path(sys.argv[1]).is_file()):
        main(sys.argv[1])
    else:
        print("File do not exist")
