import sys
from pathlib import Path

from homography import pyrs12, spaces
import time, cv2, h5py, datetime
import numpy as np
import deproject, HandShapeAnalysis


def main():
    cam = pyrs12.Camera()
    cam.start()
    print("camera started")

    print("main")
    cv2.namedWindow('Bird\'s-eyes angle', cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow('camera angle', cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow('point image', cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow('Color', cv2.WINDOW_AUTOSIZE)

    #analyser = HandShapeAnalysis.Depth2Finger(pcl_center, trans_matrix)
    # analyser.set_show(debug = False,
    # finger_point_image = True, hull_image = False, detected_defect = True, cluster_point = False, possibility_modal = False)

    pcl_center, trans_matrix = partten(cam)
    analyser2 = HandShapeAnalysis.Depth2Finger(pcl_center, trans_matrix)
    analyser2.set_show(debug=False,
                       finger_point_image=True, hull_image=False, detected_defect=True, cluster_point=False, possibility_modal=False)

    from keras.models import load_model
    model = load_model('depth_angle_model.hdf5')
    try:
        while True:
            cam.wait_for_frames()

            # Convert images to numpy arrays
            color_image, depth_image, _ = cam.cdi

            # analyser.renew_high(depth_frame[i])
            areas = analyser2.renew_both(depth_image)
            if(len(areas) == 0):
                high_angle = np.hstack((analyser2.H_finger_point_image, analyser2.H_detected_defect))
                camera_angle = np.hstack((analyser2.D_finger_point_image, analyser2.D_detected_defect))
                cv2.imshow('Color', cv2.resize(color_image, (0, 0), fx=0.5, fy=0.5))
                cv2.imshow('Bird\'s-eyes angle', high_angle)
                cv2.imshow('camera angle', camera_angle)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
                continue

            x_predicts = analyser2.predict_return(areas)
            y_predict = model.predict(x_predicts)
            zip_sturcture = np.concatenate(
                (y_predict, analyser2.relative_coordinate), axis=1)

            for r in zip_sturcture:
                if (r[0] == 1):  # if touching change color
                    p = np.concatenate(((r[1]*350).reshape(-1,1),
                     (r[2]*400).reshape(-1,1)),axis = 1)
                    cv2.drawContours(analyser2.finger_point_image, p.reshape(
                        -1, 1, 2).astype(int), -1, (0, 0, 255), 10)

            high_angle = np.hstack((analyser2.H_finger_point_image, analyser2.H_detected_defect))
            camera_angle = np.hstack((analyser2.D_finger_point_image, analyser2.D_detected_defect))
            finger_point_image = analyser2.finger_point_image
            finger_point_image = cv2.resize(finger_point_image, (0, 0), fx=0.8, fy=0.8)

            cv2.imshow('Color', cv2.resize(color_image, (0, 0), fx=0.5, fy=0.5))
            cv2.imshow('Bird\'s-eyes angle', high_angle)
            cv2.imshow('camera angle', camera_angle)
            cv2.imshow('point image', finger_point_image)
            cv2.waitKey(0)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cam.stop()
                break
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    finally:
        cam.stop()

def partten(cam):
    print("wasting first 30 loop, please don't move the camera")
    for i in range(30):
        cam.wait_for_frames()
        print(i,". ", end="", flush=True)
    print()
    #partten
    pcl_center, trans_matrix = get_partten(cam)
    while  np.isnan(trans_matrix[0][0]):
        pcl_center, trans_matrix = get_partten(cam)
    print("partten recorded")
    print("pcl_center")
    print(pcl_center)
    print("trans_matrix")
    print(trans_matrix)
    return pcl_center, trans_matrix

def get_partten(cam):
    cam.wait_for_frames()
    scene = spaces.Scene(cam, './homography/rgb-marker.png')
    pcl_center = scene.system.pcl_center
    trans_matrix = scene.system.trans_matrix
    return pcl_center, trans_matrix

if __name__ == "__main__":
    main()
