import numpy as np
import cv2


# Capture frame-by-frame
frame = cv2.imread('/Users/tom/Pictures/illust_70181279_20180819_015131.png',cv2.IMREAD_COLOR)

# Our operations on the frame come here



resized = cv2.resize(frame,None,fx=.5, fy=.5, interpolation = cv2.INTER_CUBIC)

gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
noise_reduce = cv2.blur(resized,(3,3))

canny_edage = cv2.Canny(noise_reduce,40,50)
canny_edage = cv2.bitwise_not(canny_edage)

ret,thresh1 = cv2.threshold(canny_edage,127,1,cv2.THRESH_BINARY)
masked = resized * thresh1[:,:,None]
#resized(size,size,N_of_color)*thresh1(size,size,0_or_1), so no color(N_of_color=0) or have color

# Display the resulting frame
cv2.imshow('frame',masked)

cv2.waitKey(0)
cv2.destroyAllWindows()
# When everything done, release the capture
