import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here



    resized = cv2.resize(frame,None,fx=.5, fy=.5, interpolation = cv2.INTER_CUBIC)

    gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
    noise_reduce = cv2.blur(resized,(3,3))

    canny_edage = cv2.Canny(noise_reduce,40,50)
    canny_edage_reverse = cv2.bitwise_not(canny_edage)
    ret,thresh1 = cv2.threshold(canny_edage_reverse,127,1,cv2.THRESH_BINARY)
    masked = resized * (thresh1[:,:,None].astype(canny_edage_reverse.dtype))
    # Display the resulting frame
    cv2.imshow('masked',masked)
    cv2.imshow('canny_edage',canny_edage_reverse)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
