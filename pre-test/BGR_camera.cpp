// include the librealsense C++ header file
#include <librealsense2/rs.hpp>
#include <iostream>
// include OpenCV header file
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main()
{
    //Contruct a pipeline which abstracts the device
    rs2::pipeline pipe;
    cout<<"pipe\r";
    //Create a configuration for configuring the pipeline with a non default profile
    rs2::config cfg;
    cout<<"cfg\r";
    //Add desired streams to configuration
    cfg.enable_stream(RS2_STREAM_COLOR, 640, 480, RS2_FORMAT_BGR8, 30);
    cout<<"enable_stream\r";
    //Instruct pipeline to start streaming with the requested configuration
    pipe.start(cfg);
    cout<<"start";

    rs2::frameset frames;
    namedWindow("Display Image", WINDOW_AUTOSIZE );
    while(1){

      frames = pipe.wait_for_frames();
      rs2::frame color_frame = frames.get_color_frame();
      Mat color(Size(640, 480), CV_8UC3, (void*)color_frame.get_data(), Mat::AUTO_STEP);
      imshow("Display Image", color);
      if(waitKey(30) >= 0) return 0;
    }
}
