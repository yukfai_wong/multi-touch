from sklearn import pipeline

from sys import path
path.insert(1,'/home/antoine/Documents/owndev/fistwriter_project/')

from fistwriter.process import ml
from fistwriter.utils import clientserver as cs

import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


import time

import numpy as np

import transitions as tr


class LastTrace(object):
    """docstring for LastTrace"""
    def __init__(self, dim):
        super(LastTrace, self).__init__()
        self.dim = dim
        self.data = np.empty((0,self.dim))

    def new_sample(self, sample):
        self.data = np.r_[self.data, sample]

    def reset(self):
        self.data = np.empty((0,self.dim))


class Publisher(object):
    """docstring for Sampler"""
    def __init__(self):
        super(Publisher, self).__init__()

        self.pub = cs.SimplePublisher(port = "5556")

        self.state_mapping = {
            'hovering_enter' : ('hover', 'enter'),
            'hovering' : ('hover', 'move'),
            'hovering_exit' : ('hover', 'exit'),
            'touching_enter' : ('touch', 'down'),
            'touching' : ('touch', 'move'),
            'touching_exit' : ('touch', 'up'),
        }

    def send(self, state_name, xy):
        send_state = self.state_mapping[state_name]
        msg = {'state':send_state, 'coord':np.array(xy).tolist()}
        logger.debug("publish {}".format(send_state))
        self.pub.send(msg)


class Tracker(object):
    """docstring for Tracker"""
    def __init__(self):
        super(Tracker, self).__init__()


        ## this should come from the ml package

        roipip = pipeline.Pipeline([
            ('voxelgridfilter', ml.VoxelGridFilter(leaf_size=0.002)),
            ('roiextractor', ml.RoiExtractor()),
            ('fingerPlaneExtractor', ml.FingerPlaneExtractor()),
            ])

        self.roipip = roipip
        ## touch and position classifier
        self.touchclassifier = ml.TouchClassifier(
            '/home/antoine/Documents/owndev/fistwriter_project/analysis/step_class/model_touch_2017-03-08_11:26:47_copy.hdf5'
            # '/home/antoine/Documents/owndev/fistwriter_project/analysis/step_class/model_touch_2017-03-07_15:30:05.hdf5'
            # model_touch_2017-03-06_20:12:00.hdf5'
            # '/home/antoine/Documents/owndev/fistwriter_project/analysis/step_a/model_touch_2017-03-02_18:04:11.hdf5'
            )
        self.touchregressor = ml.TouchRegressor()


    def track(self, pc):
        self.roi_t0 = time.time()
        roi = self.roipip.transform([pc]).next()
        self.roi_t1 = time.time()
        finger, plane = roi

        ## get the x,y,t
        if ((finger == 0).all()) and ((plane == 0).all()):
            touch = 0.0
            x, y = -1.0, -1.0
        else:
            touch = self.touchclassifier.predict([roi])[0,0]
            x, y = self.touchregressor.predict([roi])[0]

        return touch, x, y



class TouchFilter(object):
    def __init__(self, save_size = 50, median_size = 5):

        self.save_size = save_size
        self.median_size = median_size

        self.touch_data = np.zeros(save_size)
        self.xy = np.zeros((save_size,2))

    def new_frame(self, touch, x, y):

        touch = float(touch)

        self.touch_data = np.roll(self.touch_data, -1)
        self.touch_data[-1] = touch
        self.xy = np.roll(self.xy, -1)
        self.xy[-1] = (x,y)

        filteredtouch = np.mean(self.touch_data[-self.median_size:])

        return filteredtouch, x, y


class TouchState(tr.Machine):
    """3 state touch button with out-of-range, hover and touch state.
    The state changes are triggered by a tuple (touch, x, y).
    """
    def __init__(self, callback, *args, **kwargs):

        ## this should be set from the ROC curve
        self.THRESHOLD = 0.3

        states = [
            tr.State('outofrange'),
            tr.State('hovering_enter', on_enter='to_hovering', on_exit=callback),
            tr.State('hovering',       on_enter=callback),
            tr.State('hovering_exit',  on_enter='to_outofrange', on_exit=callback),
            tr.State('touching_enter', on_enter='to_touching', on_exit=callback),
            tr.State('touching',       on_enter=callback),
            tr.State('touching_exit',  on_enter='to_hovering', on_exit=callback),
        ]

        transitions = [
            {'trigger': 'nohand' , 'source': 'outofrange', 'dest': 'outofrange'},
            {'trigger': 'nohand' , 'source': 'hovering', 'dest': 'hovering_exit'},

            {'trigger': 'notouch' , 'source': 'outofrange', 'dest': 'hovering_enter'},
            {'trigger': 'notouch' , 'source': 'hovering', 'dest': 'hovering'},
            {'trigger': 'notouch' , 'source': 'touching', 'dest': 'touching_exit'},

            {'trigger': 'touch', 'source': 'hovering', 'dest': 'touching_enter'},
            {'trigger': 'touch', 'source': 'touching', 'dest': 'touching'},

            ## failsafe
            {'trigger': 'nohand' , 'source': 'touching', 'dest': 'touching_exit', 'after': 'nohand'},
            {'trigger': 'touch' , 'source': 'outofrange', 'dest': 'hovering_enter', 'after': 'notouch'},
        ]

        tr.Machine.__init__(self,
                states = states,
                transitions = transitions,
                initial = 'outofrange',
                send_event = True,
                queued = True,
                ignore_invalid_triggers = True,
                )

    def new_frame(self, touch, xy):
        touch = float(touch)

        ## change the threshold depending on state
        ## harder touch to start, easier to leave
        if self.state == 'hovering':
            self.THRESHOLD = 0.7
        elif self.state == 'touching':
            self.THRESHOLD = 0.3

        if (xy == np.array([-1,-1])).all():
            self.nohand()
        else:
            if touch > self.THRESHOLD:
                self.touch()
            else:
                self.notouch()


from pykalman import KalmanFilter
def get_kf_2d(x,y):

    dt = 1./30
    D = 1
    C = 1
    Q = 7 * 1

    transition_matrix = np.array([[1,0,dt,0], [0,1,0,dt], [0,0,D,0], [0,0,0,D]])
    observation_matrix = np.array([[1, 0, 0, 0], [0,1,0,0]])
    initial_transition_covariance = np.eye(4) * C
    initial_observation_covariance = np.eye(2) * Q
    initial_state_mean = np.zeros((4,)) * C
    initial_state_covariance = np.eye(4) * C

    return KalmanFilter(
    transition_matrices = transition_matrix,
    observation_matrices = observation_matrix,
    transition_covariance = initial_transition_covariance,
    observation_covariance = initial_observation_covariance,
    initial_state_mean = np.r_[x, y, 0, 0],
    initial_state_covariance = initial_state_covariance,
    n_dim_state=4, n_dim_obs=2,
    em_vars=[
      'transition_matrices', 'observation_matrices',
      'transition_covariance', 'observation_covariance',
      'initial_state_covariance'
    ])

