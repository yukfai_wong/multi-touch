import numpy as np

import transitions as tr
from fistwriter.utils import clientserver as cs

import logging
logger = logging.getLogger(__name__)

import cv2

def rescale_depth(dm, MAX_DEPTH=8e2):
    dm = dm.copy()
    dm[dm>MAX_DEPTH] = 0
    min_ = 0
    max_ = MAX_DEPTH
    dm = (dm - min_) * (255./(max_-min_))
    dm = dm.astype(np.uint8)
    dm = cv2.applyColorMap(dm, cv2.COLORMAP_RAINBOW)
    return dm
