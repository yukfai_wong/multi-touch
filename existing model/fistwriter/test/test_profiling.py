import time

import pyrealsense as pyrs
from pyrealsense.constants import rs_ivcam_preset


from sys import path
path.insert(1,'/home/antoine/Documents/owndev/fistwriter_project/')
from fistwriter.process import spaces

pyrs.start()

cam = pyrs.Device(ivcam_preset = rs_ivcam_preset.RS_IVCAM_PRESET_GESTURE_RECOGNITION)
cam.set_device_option(pyrs.constants.rs_option.RS_OPTION_SR300_AUTO_RANGE_UPPER_THRESHOLD, 2500)
time.sleep(5)


scene = spaces.Scene(cam, './utils/rgb-marker.png', shape='S4')

for i in range(100):
    cam.wait_for_frame()
    pc = cam.points

    t0 = time.time()

    norm = scene.system.normalise(pc)
    filt = scene._filter(norm)
    scal = scene._scale(filt)

    print str((time.time()-t0)*1000)[:4]
