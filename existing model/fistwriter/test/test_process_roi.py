import time
import numpy as np
import multiprocessing as mp

from sys import path
path.insert(1,'/home/antoine/Documents/owndev/fistwriter_project/')

import fistwriter.ml as ml

import  pyrealsense as pyrs
pyrs.start()
dev = pyrs.Device()

dev.wait_for_frame()
pc = dev.points

def process(pc):
    roi = ml.roipip.transform([pc]).next()

p = mp.Process(target=process, args=(pc.reshape(-1,3),))
t0 = time.time()
p.start()
p.join()
print str((time.time()-t0)*1000)[:4]

for i in range(10):
    dev.wait_for_frame()
    pc = dev.points

    t0 = time.time()
    ml.roipip.transform([pc.reshape(-1,3)]).next()
    print str((time.time()-t0)*1000)[:4]
