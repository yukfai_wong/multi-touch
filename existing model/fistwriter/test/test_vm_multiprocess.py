import time

import logging

logFormatter = logging.Formatter(fmt="%(asctime)s [%(levelname)-5.5s]  %(message)s",
                                 datefmt='%m-%d %H:%M')

import sys

print __name__, __file__

# sys.exit(0)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

fileHandler = logging.FileHandler(__file__+'.log')
fileHandler.setLevel(logging.DEBUG)
fileHandler.setFormatter(logFormatter)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.DEBUG)
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)



import pyrealsense as pyrs
from pyrealsense.constants import rs_ivcam_preset
from pyrealsense import offline
offline.load_depth_intrinsics('610205001689')

import threading
import multiprocessing as mp
import Queue

import numpy as np


from sys import path
path.insert(1,'/home/antoine/Documents/owndev/fistwriter_project/')

import fistwriter.ml as ml
ml.logger.setLevel(logging.INFO)
from fistwriter.process import spaces
from fistwriter import process


import cv2

import datetime
datetime_format = "%Y-%m-%d_%H:%M:%S:%f"

def time_since_x_in_ms(x):
    return "{:4}".format(int((time.time()-x)*1000%1000))



class Sampler(mp.Process):
    def __init__(self, loop_event):
        super(Sampler, self).__init__()
        self.loop_event = loop_event
        self.rb = process.RingBuffer(100, 4)

    def run(self):

        self.context = zmq.Context()
        self.in_queue = self.context.socket(zmq.PULL)
        self.in_queue.bind("tcp://127.0.0.1:5558")

        self.tick = self.context.socket(zmq.PULL)  # could be a sub
        self.tick.connect("tcp://127.0.0.1:5559")


        self.poller = zmq.Poller()
        self.poller.register(self.in_queue, zmq.POLLIN)
        self.poller.register(self.tick, zmq.POLLIN)

        logger.debug("{} {}".format('Sampler started', self.pid))
        # do some initialization here

        while self.loop_event:
            socks = dict(self.poller.poll())

            ## get tick
            if socks.get(self.tick) == zmq.POLLIN:
                data = self.tick.recv_json()
                fn, t = data['fn'], data['t']
                # logger.debug("tick {} {}".format(fn, time.time()-t))


            ## get data
            if socks.get(self.in_queue) == zmq.POLLIN:

                t0 = time.time()

                work = self.in_queue.recv_json()
                fn, touch, x, y = work['fn'], work['touch'], work['x'], work['y']

                logger.debug("{} {} {} {} {}".format('*', self.pid, fn, time_since_x_in_ms(t0), datetime.datetime.now().strftime(datetime_format)))

                ## stack the last value and plot the number of dropped frames
                fn_1, _, _, _ = self.rb.get_last()

                if (fn - fn_1) > 2:
                    logger.debug("dropped frame {} {}".format(fn, fn_1))

                self.rb.new_sample(np.array([fn, touch, x, y]))

        logger.debug('Sampler exiting')


class Worker(mp.Process):
    def __init__(self, fun, arrays):
        super(Worker, self).__init__()
        self.fun = fun
        self.arrays = arrays

        ## roi extractor
        self.roipip = ml.roipip
        self.touchclassifier = ml.TouchClassifier(
            '/home/antoine/Documents/data/0/model_touch_2017-01-04_18:37:45.hdf5')
        self.touchregressor = ml.TouchRegressor()

        self.loop_event = True

    def run(self):


        self.context = zmq.Context()
        self.in_queue = self.context.socket(zmq.PULL)
        self.in_queue.connect("tcp://127.0.0.1:5557")

        self.out_queue = self.context.socket(zmq.PUSH)
        self.out_queue.connect("tcp://127.0.0.1:5558")

        logger.debug("{} {}".format('Worker started', self.pid))


        while self.loop_event:
            work = self.in_queue.recv_json()
            fn, cnt = work['fn'], work['cnt']

            if type(cnt) is int:
                t0 = time.time()

                data = self.arrays[cnt]
                data = np.frombuffer(data, dtype=np.uint16)

                t1 = time.time()

                data = offline.deproject_depth(data.reshape(480,640))
                data = self.fun(data)
                roi = self.roipip.transform([data]).next()

                t2 = time.time()

                finger, plane = roi
                if ((finger == 0).all()) and ((plane == 0).all()):
                    touch = 0.0
                    x, y = -1.0, -1.0
                else:
                    touch = self.touchclassifier.predict([roi])[0,0]
                    touch = float(touch.tolist())
                    x, y = self.touchregressor.predict([roi])[0]
                    x, y = float(x), float(y)

                t3 = time.time()

                logger.debug("{} {} {} {} {} {} {}".format('-',
                    self.pid, fn,
                    time_since_x_in_ms(t0), time_since_x_in_ms(t1),
                    time_since_x_in_ms(t2), time_since_x_in_ms(t3),))
                    # datetime.datetime.now().strftime(datetime_format)))

                logger.debug("{} {} {} {} {}".format('-', self.pid, fn, time_since_x_in_ms(t0), datetime.datetime.now().strftime(datetime_format)))

                self.out_queue.send_json({'touch':touch, 'x':x, 'y':y, 'fn':fn})

            else:
                logger.debug('must stop')
                self.loop_event = False


import os
import ctypes
import zmq

class VirtualMouse(threading.Thread):
    """docstring for VirtualMouse"""
    def __init__(self, camera, pattern):
        super(VirtualMouse, self).__init__()
        self.cam = camera

        ## create the scene
        self.scene = spaces.Scene(camera, pattern, shape='S4')

        ## job queue
        self.context = zmq.Context()
        self.worker_queue = self.context.socket(zmq.PUSH)
        self.worker_queue.bind("tcp://127.0.0.1:5557")

        ## tick
        self.tick = self.context.socket(zmq.PUSH)
        self.tick.bind("tcp://127.0.0.1:5559")

        ## local stuff
        self.loop_event = threading.Event()
        self.loop_event.set()
        self.pid = os.getpid()

        ## worker processing
        self.nworkers = 4
        self.cnt = 0
        self.arrays = []

        for i in range(self.nworkers):
            self.arrays.append(mp.Array(ctypes.c_uint16, (480*640), lock=False))
        for i in range(self.nworkers):
            Worker(self.scene.get_volume, self.arrays).start()

        ## sampler
        self.sampler_event = mp.Event()
        self.sampler_event.set()
        Sampler(self.sampler_event).start()

        self.start()


    def stop(self):

        self.loop_event.clear()
        self.sampler_event.clear()
        for i in range(self.nworkers): self.worker_queue.send_json({'fn':0, 'cnt':None})

        logger.debug('done with stop')


    def run(self):
        while self.loop_event.is_set():
            self.cnt += 1
            self.cnt %= self.nworkers

            ## poll for frame - this is the timer tick for our application
            if self.cam.poll_for_frame():

                fn = self.cam.get_frame_number(pyrs.rs_stream.RS_STREAM_DEPTH)
                self.tick.send_json({'fn':fn, 't':time.time()})
                t0 = time.time()

                arr = np.frombuffer(self.arrays[self.cnt], dtype=np.uint16)
                arr[:] = self.cam.depth.reshape(-1)

                logger.debug("{} {} {} {} {}".format('+', self.pid, fn, time_since_x_in_ms(t0), datetime.datetime.now().strftime(datetime_format)))

                self.worker_queue.send_json({'fn':fn, 'cnt':self.cnt})

            time.sleep(1./100)

        logger.debug('vm exit')


import argparse

def main(args):

    ## start the camera
    pyrs.start()
    cam = pyrs.Device(ivcam_preset = rs_ivcam_preset.RS_IVCAM_PRESET_GESTURE_RECOGNITION)
    cam.set_device_option(pyrs.constants.rs_option.RS_OPTION_SR300_AUTO_RANGE_UPPER_THRESHOLD, 2500)
    time.sleep(5)

    ## show the homography
    # from fistwriter.tracker import show_homography
    # pattern = './utils/rgb-marker.png'
    # show_homography.main(cam, pattern)



    vm = VirtualMouse(cam, args.p)

    while True:
        # print 'new loop'
        time.sleep(1./100)
        # time.sleep(0)
        # t, x, y = vm.track()
        # vm.filter(t,x,y)
        # vm.publish()


        # Check key input
        k = cv2.waitKey(1)

        # cv2.imshow('depth', process.rescale_depth(cam.depth))
        ## plot
        img = vm.scene.pattern.pattern.copy()
        # img = vm.scene.draw_point(img, vm.xy[0], vm.xy[1])
        cv2.imshow('', img) #cv2.cvtColor(img, cv2.COLOR_GRAY2BGR))

        key = chr(k & 255)
        if key == 'q':
            break
        if key == 'd':
            import ipdb; ipdb.set_trace()

    logger.debug('done with loop')
    vm.stop()

    cv2.destroyAllWindows()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Live tracking.')
    parser.add_argument('-p', metavar='pattern', help='optical pattern', required=True)

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    main(args)
