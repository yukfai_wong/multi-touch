import numpy as np
import cv2
import pyrealsense2 as pyrs
import time
import argparse

import matplotlib.pyplot as plt

from process import spaces, rescale_depth
#from utils import vtk_plot
from viewers import pointcloudstreamer as pcs

import ml
from scipy.spatial import ckdtree

import logging
# FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
FORMAT = '%(asctime)s\t%(levelname)s %(module)s: %(message)s'
logging.basicConfig(format = FORMAT)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

ml_logger = ml.logger
ml_logger.setLevel(logging.ERROR)


from pyrealsense.constants import rs_ivcam_preset

pyrs.start()
time.sleep(2)
cam = pyrs.Device(ivcam_preset = rs_ivcam_preset.RS_IVCAM_PRESET_GESTURE_RECOGNITION,
                  with_burning=300)
cam.wait_for_frame()

# np.save('pointcloud', cam.pointcloud)
# np.save('points', cam.points)
# np.save('depth', cam.depth)

scene = spaces.Scene(cam, './utils/rgb-marker.png')
roipip = ml.roipip

roiextractor = roipip.steps[0][1]

# import ipdb; ipdb.set_trace()

# vtk_plot.plot_objects(scene.volume, axis=True)

# d = np.load('depth.npy')
# dcoord = np.rollaxis(np.indices(d.shape), 0, 3).reshape(-1,2)
# d3d = np.c_[dcoord, d.reshape(-1,1)].astype(np.float32)

def main_non_blocking():
    awp_pc = pcs.ActorWrapper(640*480*3)

    awp_fingers = pcs.ActorWrapper(10000*3)
    awp_plane = pcs.ActorWrapper(20000*3)
    awp_finger = pcs.ActorWrapper(2000*3)

    viz = pcs.Visualisation(awp_pc, awp_fingers, awp_plane, awp_finger)
    viz.start()

    cnt = 0
    while True:
        cnt += 1



        # print cnt
        pc = scene.volume
        awp_pc.update_mparr(pc)

        ## get the ROI
        roi = roipip.transform([pc]).next()
        fingers, plane = roi


        if fingers.shape[0] < 10000:
            awp_fingers.update_mparr(np.r_[fingers, np.zeros((10000-fingers.shape[0], 3))])
        if plane.shape[0] < 20000:
            awp_plane.update_mparr(np.r_[plane, np.zeros((20000-plane.shape[0], 3))])

        t0 = time.time()
        fkdt = ckdtree.cKDTree(fingers)
        seed = fingers[:,1].argmax()


        finger_idx = ml.grow(fkdt, fingers, set([seed]), set([seed]))
        finger = fingers[list(finger_idx)]
        t1 = time.time()

        if finger.shape[0] < 2000:
            awp_finger.update_mparr(np.r_[finger, np.zeros((2000-finger.shape[0], 3))])

        logger.debug("{} plane {} fingers {} finger {}".format(
            str((t1-t0)*1000)[:4],
            plane.shape[0], fingers.shape[0], finger.shape[0]))

        cm = cam.colour
        cv2.imshow('cm', cv2.cvtColor(cm, cv2.COLOR_RGB2BGR))
        cv2.imshow('dm', cv2.cvtColor(rescale_depth(roiextractor.frame), cv2.COLOR_RGB2BGR))

        # Check key input
        k = cv2.waitKey(1)
        key = chr(k & 255)
        if key == 'q':
            break
        if key == 'd':
            import ipdb; ipdb.set_trace()

main_non_blocking()
