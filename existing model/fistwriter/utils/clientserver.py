
import zmq
import json

class SimplePublisher(object):
    def __init__(self, port):
        super(SimplePublisher, self).__init__()
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PUB)
        self.socket.bind("tcp://*:%s" % port)

    def send(self, data):
        data_json = json.dumps(data)
        self.socket.send_json(data_json)

    def close(self):
        self.socket.close()
        self.context.term()

