import numpy as np
import vtk
from . import pointobject
from matplotlib.pyplot import cm


class ProcessingError(Exception):
    """Error thrown during the processing in case the processing chain needs to be exited."""
    def __init__(self, msg, value):
        self.msg = msg
        self.value = value
    def __str__(self):
        return repr(self.msg)


def plot_objects(*args, **kwargs):
    """Plot n objects using VTK."""
    ren = vtk.vtkRenderer()

    colors = cm.rainbow(np.linspace(0,1,len(args)))
    objs = []
    for i, arg in enumerate(args):

        # make sure input is ok
        if len(arg.shape) > 2:
            arg = arg.reshape(-1,3)
        if not arg.flags.contiguous:
            arg = np.ascontiguousarray(arg)

        obj = pointobject.VTKObject()
        obj.CreateFromArray(arg)

        color_cur = (colors[i][:3]*255).astype(int)
        obj.AddColors(np.ones((arg.shape[0],1), dtype=int) * color_cur)

        ren.AddActor(obj.GetActor())

        objs.append(obj)

    if ('axis' in kwargs) and (kwargs['axis'] is not False):
        axes = pointobject.VTKObject()
        axes.CreateAxes(1)
        ren.AddActor(axes.GetActor())

    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(ren)
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renWin)
    style = vtk.vtkInteractorStyleTrackballCamera()
    iren.SetInteractorStyle(style)
    iren.Initialize()
    iren.Start()


## this should be hooked up with a GUi to plot and show the datasets
# f = h5py.File('foo.hdf5','r')
# f.visititems(print_attrs)
# import h5py

def print_attrs(name, obj):
    print(name)
    for key, val in obj.attrs.iteritems():
        print("    %s: %s" % (key, val))

import sys
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

import logging

def setup_logging(logger, filename):
    FORMAT = '%(asctime)s\t%(levelname)s %(module)s:%(lineno)d %(message)s'
    logFormatter = logging.Formatter(fmt=FORMAT,
                                     datefmt='%m-%d %H:%M')

    fileHandler = logging.FileHandler(filename+'.log')
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(logFormatter)
    logger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)
