import time

import numpy as np
import multiprocessing as mp
import ctypes
from matplotlib.pyplot import cm

import vtk
import vtk.util.numpy_support as vtk_np


from sys import path
path.insert(1,'/home/antoine/Documents/owndev/fistwriter_project/')

from fistwriter.utils.pointobject import VTKObject

class Visualisation(mp.Process):
    def __init__(self, *args):
        super(Visualisation, self).__init__()

        self.ren = vtk.vtkRenderer()

        colors = cm.rainbow(np.linspace(0,1,len(args)))

        self.actorWrappers = []
        for i, arg in enumerate(args):
            color_cur = (colors[i][:3]*255).astype(int)
            arg.add_colors(np.ones((arg.nparray.shape[0],1), dtype=int) * color_cur)

            self.actorWrappers.append(arg)
            self.ren.AddActor(arg.actor)

        if False:#axis:
            self.axes = VTKObject()
            self.axes.CreateAxes(0.1)
            self.ren.AddActor(self.axes.GetActor())

        self.ren.SetBackground(1,1,1)


    def run(self):
        self.renWin = vtk.vtkRenderWindow()
        self.renWin.AddRenderer(self.ren)

        self.iren = vtk.vtkRenderWindowInteractor()
        self.iren.SetRenderWindow(self.renWin)
        self.iren.Initialize()

        self.style = vtk.vtkInteractorStyleTrackballCamera()
        self.iren.SetInteractorStyle(self.style)

        self.iren.AddObserver("TimerEvent", self.update_visualisation)
        self.timerId = self.iren.CreateRepeatingTimer(10) # ms

        self.iren.Start()
        self.iren.DestroyTimer(self.timerId)

    def update_visualisation(self, obj=None, event=None):
        for actorWrapper in self.actorWrappers:
            awp = actorWrapper
            awp.nparray[:] = np.frombuffer(awp.mparr.get_obj()).reshape(-1,3)
            awp.pd.Modified()

        self.ren.GetRenderWindow().Render()


class ActorWrapper(object):
    def __init__(self, size, pointsize=3):
        super(ActorWrapper, self).__init__()

        self.mparr = mp.Array(ctypes.c_double, size)
        self.nparray = np.frombuffer(self.mparr.get_obj()).reshape(-1,3)

        nCoords = self.nparray.shape[0]
        nElem = self.nparray.shape[1]

        self.verts = vtk.vtkPoints()
        self.cells = vtk.vtkCellArray()
        self.scalars = None

        self.pd = vtk.vtkPolyData()
        self.verts.SetData(vtk_np.numpy_to_vtk(self.nparray))
        self.cells_npy = np.vstack([np.ones(nCoords,dtype=np.int64),
                               np.arange(nCoords,dtype=np.int64)]).T.flatten()
        self.cells.SetCells(nCoords,vtk_np.numpy_to_vtkIdTypeArray(self.cells_npy))
        self.pd.SetPoints(self.verts)
        self.pd.SetVerts(self.cells)

        self.mapper = vtk.vtkPolyDataMapper()
        self.mapper.SetInputDataObject(self.pd)

        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)
        self.actor.GetProperty().SetRepresentationToPoints()
        self.actor.GetProperty().SetColor(0.0,1.0,0.0)
        self.actor.GetProperty().SetPointSize(pointsize)

    def add_colors(self, cdata):
        nColors = cdata.shape[0]
        nDim = cdata.shape[1]

        self.colors_npy = cdata.copy().astype(np.uint8)
        self.colors = vtk_np.numpy_to_vtk(self.colors_npy)

        self.colors = vtk.vtkUnsignedCharArray()
        self.colors.SetNumberOfComponents(3)
        self.colors.SetName("Colors")

        for i in range(nColors):
            self.colors.InsertNextTuple(tuple(cdata[i,:]))

        self.pd.GetPointData().SetScalars(self.colors)
        self.pd.Modified()

    def update_mparr(self, data):
        if data.shape > 1:
            data = data.reshape(-1)

        if data.size != self.nparray.size:
            z = np.zeros(self.nparray.size-data.size)
            data = np.hstack((data,z))

        mpdat = np.frombuffer(self.mparr.get_obj())
        mpdat[:] = data


def main(fun):
    import cv2

    awp_pc = ActorWrapper(640*480*3)
    viz = Visualisation(awp_pc)
    viz.start()


    while True:
        # dev.wait_for_frame()
        # pc = dev.pointcloud
        # cm = dev.colour
        pc, cm = fun()

        awp_pc.update_mparr(pc)
        cv2.imshow('cm', cv2.cvtColor(cm, cv2.COLOR_RGB2BGR))

        # Check key input
        k = cv2.waitKey(1)
        key = chr(k & 255)
        if key == 'q':
            break
        if key == 'd':
            import ipdb; ipdb.set_trace()


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Live tracking.')
    parser.add_argument('-f', metavar='fun', help='function that generates the data',
        required=False, default=False)
    args = parser.parse_args()

    if args.f:
        fun = args.f
    else:
        import pyrealsense as pyrs
        from pyrealsense.constants import rs_ivcam_preset, rs_option

        pyrs.start()
        time.sleep(2)
        dev = pyrs.Device()
        dev.set_device_option(rs_option.RS_OPTION_F200_FILTER_OPTION, 2)

        def fun():
            dev.wait_for_frame()
            pc = dev.pointcloud
            cm = dev.colour
            return pc, cm

    main(fun)



# def main_blocking():
#     import pyrealsense as pyrs
#     pyrs.start()
#     time.sleep(2)
#     pc = pyrs.get_pointcloud().reshape(-1,3)

#     actorWrapper = VTKActorWrapper(pc)
#     ren = vtk.vtkRenderer()
#     ren.AddActor(actorWrapper.actor)

#     axes = pointobject.VTKObject()
#     axes.CreateAxes(1)
#     ren.AddActor(axes.GetActor())

#     renWin = vtk.vtkRenderWindow()
#     renWin.AddRenderer(ren)

#     update_on = threading.Event()
#     update_on.set()

#     def update_actor(pc, actorWrapper, fun, update_on):
#         while (update_on.is_set()):
#             time.sleep(0.01)
#             pc[:] = fun()
#             actorWrapper.pd.Modified()

#     def fun():
#         return pyrs.get_pointcloud().reshape(-1,3)

#     thread = threading.Thread(target=update_actor, args=(pc, actorWrapper, fun, update_on))
#     thread.start()

#     def update_visualisation(obj=None, event=None):
#         time.sleep(0.01)
#         ren.GetRenderWindow().Render()

#     ## IREN
#     iren = vtk.vtkRenderWindowInteractor()
#     iren.SetRenderWindow(renWin)
#     iren.Initialize()

#     style = vtk.vtkInteractorStyleTrackballCamera()
#     iren.SetInteractorStyle(style)

#     iren.AddObserver("TimerEvent", update_visualisation)
#     dt = 1 # ms
#     timer_id = iren.CreateRepeatingTimer(dt)

#     iren.Start()
#     update_on.clear()
#     thread.join()