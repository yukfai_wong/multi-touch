import numpy as np
import cv2
import pyrealsense as pyrs
import time
import argparse

from sys import path
path.insert(1,'/Users/antoine/Documents/owndev/fistwriter_project/')
from fistwriter.process.spaces import Pattern
from fistwriter.utils import ProcessingError

def main(cam, pattern):
    cam.wait_for_frames()
    image = cv2.cvtColor(cam.cad, cv2.COLOR_RGB2BGR)

    pattern = Pattern(pattern, image)

    while True:

        cam.wait_for_frames()
        image = cv2.cvtColor(cam.cad, cv2.COLOR_RGB2BGR)

        ## find homography
        try:
            pattern.find_homography(image)
        except ProcessingError as e:
            continue

        ## if pattern found, draw the surface
        if pattern.mask is not None:
            pattern.draw_contours(image)
            image = pattern.draw_matches()
            ## draw keypoints and matches

        cv2.imshow("homography", image)

        # Check key input
        k = cv2.waitKey(1)
        key = chr(k & 255)
        if key == 'q':
            cv2.destroyWindow("homography")
            break
        if key == 'd':
            import ipdb; ipdb.set_trace()



if __name__ == '__main__':
    desc = 'Draw a surface and its associated pattern[s] if correctly detected.'
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument('-p', metavar='X', help='pattern 0 to find', required=True)
    parser.add_argument('-d', help='use static image instead of camera', action='store_true')
    args = parser.parse_args()

    debug = args.d
    pattern = args.p

    import pyrealsense as pyrs
    from pyrealsense.constants import rs_ivcam_preset
    pyrs.start()
    cam = pyrs.Device(ivcam_preset = rs_ivcam_preset.RS_IVCAM_PRESET_GESTURE_RECOGNITION)
    cam.set_device_option(pyrs.constants.rs_option.RS_OPTION_SR300_AUTO_RANGE_UPPER_THRESHOLD,
                          2500)
    main(cam, pattern)
    cam.stop()

