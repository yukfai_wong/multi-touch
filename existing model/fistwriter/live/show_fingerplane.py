import cv2
import time
import numpy as np


from sys import path
path.insert(1,'/home/antoine/Documents/owndev/fistwriter_project/')
from fistwriter.utils.pointcloudstreamer import Visualisation, ActorWrapper
from fistwriter.process import spaces, ml


def main(scene):
    # import ipdb; ipdb.set_trace()
    # scene = spaces.Scene(dev, pattern, shape = shape)

    awp_pointcloud = ActorWrapper(640*480*3, pointsize=1)
    awp_pc = ActorWrapper(640*480*3, pointsize=2)
    awp_plane = ActorWrapper(20000*3, pointsize=3)
    awp_fingers = ActorWrapper(10000*3, pointsize=8)
    awp_finger = ActorWrapper(10000*3, pointsize=8)

    viz = Visualisation(awp_pointcloud, awp_pc, awp_plane, awp_fingers, awp_finger)
    viz.start()

    from sklearn import pipeline
    roipip = pipeline.Pipeline([
        # ('downsizeclean', ml.DownsizeClean(leaf_size=0.002)),
        ('voxelgridfilter', ml.VoxelGridFilter(leaf_size=0.002)),
        ('roiextractor', ml.RoiExtractor()),
        ('fingerPlaneExtractor', ml.FingerPlaneExtractor()),
        ])

    fte = ml.FingerTipExtractor(ball_size=0.02)

    touchclassifier = ml.TouchClassifier(
        '/home/antoine/Documents/owndev/fistwriter_project/analysis/step_class/model_touch_2017-03-08_11:26:47_copy.hdf5'
        # '/home/antoine/Documents/owndev/fistwriter_project/analysis/step_class/model_touch_2017-03-07_15:30:05.hdf5'
        # model_touch_2017-03-06_20:12:00.hdf5'
            # '/home/antoine/Documents/owndev/fistwriter_project/analysis/step_a/model_touch_2017-03-02_18:04:11.hdf5'
            )
    touchregressor = ml.TouchRegressor()


    cv2.namedWindow("color")

    while True:
        pc = scene.volume
        awp_pointcloud.update_mparr(scene.system.normalise(scene.camera.pointcloud))
        awp_pc.update_mparr(pc)


        t0 = time.time()
        fingers, plane = roipip.transform([pc]).next()
        t1 = time.time()

        roi = fingers, plane
        if ((fingers == 0).all()) and ((plane == 0).all()):
            touch = 0.0
            x, y = -1.0, -1.0
        else:
            touch = touchclassifier.predict([roi])[0,0]
            x, y = touchregressor.predict([roi])[0]


        if ((np.array([x,y]) == np.array([-1,-1])).all()): state ='nohand'
        elif (touch < 0.5): state = 'hover'
        else: state = 'hover/touch'

        print("{} {:.2f} {:.2f} {:.2f} - {}".format(str((t1-t0)*1000).split('.')[0], touch, x, y, state))

        # import ipdb; ipdb.set_trace()
        finger = fte._transform(fingers)

        if plane.shape[0] < 20000:
            awp_plane.update_mparr(np.r_[plane, np.zeros((20000-plane.shape[0], 3))])
        if fingers.shape[0] < 10000:
            awp_fingers.update_mparr(np.r_[fingers, np.zeros((10000-fingers.shape[0], 3))])
        if finger.shape[0] < 10000:
            awp_finger.update_mparr(np.r_[finger, np.zeros((10000-finger.shape[0], 3))])

        cm = scene.camera.color
        cv2.imshow("color", cv2.cvtColor(cm, cv2.COLOR_RGB2BGR))

        # Check key input
        k = cv2.waitKey(1)
        key = chr(k & 255)
        if key == 'q':
            cv2.destroyWindow("color")
            break
        if key == 'd':
            import ipdb; ipdb.set_trace()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Live tracking.')
    parser.add_argument('-p', metavar='pattern', help='optical pattern', required=True)
    parser.add_argument('-s', metavar='size', help='scene size', required=False, default='S4')
    args = parser.parse_args()

    import pyrealsense as pyrs
    pyrs.start()

    from pyrealsense.constants import rs_ivcam_preset, rs_option
    dev = pyrs.Device(ivcam_preset = rs_ivcam_preset.RS_IVCAM_PRESET_GESTURE_RECOGNITION)
    # dev.set_device_option(rs_option.RS_OPTION_F200_FILTER_OPTION, 2)
    time.sleep(15)

    scene = spaces.Scene(dev, args.p, shape = args.s)

    dev.set_device_option(rs_option.RS_OPTION_F200_FILTER_OPTION, 2)
    dev.set_device_option(rs_option.RS_OPTION_SR300_AUTO_RANGE_UPPER_THRESHOLD, 3500)


    main(scene)

