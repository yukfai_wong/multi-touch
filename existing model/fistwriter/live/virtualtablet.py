import argparse
import time

import logging
logger = logging.getLogger()

import numpy as np
import cv2
import zmq

import pyrealsense as pyrs
from pyrealsense.constants import rs_ivcam_preset

import sys
sys.path.insert(1,'/home/antoine/Documents/owndev/fistwriter_project/')

from fistwriter.process import spaces, tracker, ml
from fistwriter.utils import clientserver as cs
from fistwriter.utils import query_yes_no, setup_logging


import multiprocessing as mp
import threading


class VirtualTablet(threading.Thread):
    def __init__(self, scene):
        super(VirtualTablet, self).__init__()
        self.scene = scene

        ## events 
        self.loop_event = threading.Event()
        self.loop_event.set()
        self.initialised = threading.Event()

        ## time keeping
        self.t0 = time.time()
        self.t1 = time.time()

        self.start()

    def stop(self): self.loop_event.clear()

    def run(self):

        self.tracker = tracker.Tracker()
        self.touchfilter = tracker.TouchFilter()
        self.touchstate = tracker.TouchState(self.transition_cb)
        self.publisher = tracker.Publisher()

        self.lasttrace = tracker.LastTrace(5)

        self.initialised.set()
        while self.loop_event.is_set():

            if self.scene.camera.poll_for_frame():
                t, x, y = self.track()
                self.filter(t, x, y)
                self.transition()
                self.t1 = time.time()
            else:
                time.sleep(0.001)


    def track(self):
        self.t0 = time.time()
        self.frame_number = self.scene.camera.get_frame_number(pyrs.rs_stream.RS_STREAM_DEPTH)
        pc = self.scene.camera.pointcloud
        pc = self.scene.get_volume(pc)
        touch, x, y = self.tracker.track(pc)
        return touch, x, y


    def filter(self, t, x, y):
        t_, x_, y_ = self.touchfilter.new_frame(t, x, y)
        self.xy = (x_, y_)
        self.t = t_
        t1 = time.time()

        logger.debug("{} {} {:.2f} {:.2f} {:.2f} {:.2f}".format(
            str((t1-self.t0)*1000)[:4], self.frame_number, x,y,t,t_))


    def transition(self):
        self.touchstate.new_frame(self.t, self.xy)


    def transition_cb(self, event):
        x, y = self.xy

        ## if new hover, create Kalman - for hover and touch
        if event.state.name == 'hovering_enter':
            self.kf = tracker.get_kf_2d(x,y)
            self.fsm = self.kf.initial_state_mean
            self.fsc = self.kf.initial_state_covariance


        if event.state.name == 'touching_enter':
            self.kf = tracker.get_kf_2d(x,y)
            self.fsm = self.kf.initial_state_mean
            self.fsc = self.kf.initial_state_covariance

            ## log
            self.lasttrace.reset()
            self.lasttrace.new_sample(np.r_[self.frame_number, self.fsm].reshape(1, -1))

        if ((event.state.name == 'hovering') or
            (event.state.name == 'touching')):
            self.fsm, self.fsc = self.kf.filter_update(self.fsm, self.fsc, self.xy)
            self.xy = self.fsm[:2]

            self.lasttrace.new_sample(np.r_[self.frame_number, self.fsm].reshape(1, -1))

            ## finally, send to the tablet
            self.publisher.send(event.state.name, self.scene.norm_coord(*self.xy))

        ## send normal if not touching
        else:
            self.publisher.send(event.state.name, self.scene.norm_coord(*self.xy))



def main(pattern, shape):

    logger.setLevel(logging.DEBUG)

    setup_logging(logger, __file__)

    from transitions import logger as tr_logger
    tr_logger.setLevel(logging.ERROR)

    pyrs.start()
    cam = pyrs.Device(ivcam_preset = rs_ivcam_preset.RS_IVCAM_PRESET_GESTURE_RECOGNITION)
                    # streams=[pyrs.stream.ColourStream(fps=30), pyrs.stream.DepthStream(fps=30), pyrs.stream.PointStream(fps=30), pyrs.stream.CADStream(fps=30)])
    # cam.set_device_option(pyrs.constants.rs_option.RS_OPTION_SR300_AUTO_RANGE_UPPER_THRESHOLD, 4500)
    from pyrealsense.constants import rs_option
    cam.set_device_option(rs_option.RS_OPTION_SR300_AUTO_RANGE_UPPER_THRESHOLD, 3500)
    cam.set_device_option(rs_option.RS_OPTION_F200_FILTER_OPTION, 2)
    time.sleep(10)


    from fistwriter.live import show_homography, show_fingerplane, show_scene
    scene = spaces.Scene(cam, pattern, shape)


    show_scene.main(scene)
    show_fingerplane.main(scene)



    if not query_yes_no('calibration ok ?'): sys.exit(0)


    vm = VirtualTablet(scene)
    while not vm.initialised.is_set(): time.sleep(0.5)

    print  'vm init ok'


    ## create vis image
    pattern_rgb = cv2.cvtColor(scene.pattern.pattern, cv2.COLOR_GRAY2BGR)
    from PIL import Image
    patt = Image.fromarray(pattern_rgb)
    scen = Image.new('RGB', (scene.widthPxl, scene.heightPxl), (255, 255, 255))

    max_width = np.max([patt.width, scene.widthPxl])
    max_heigth = np.max([patt.height, scene.heightPxl])

    fina = Image.new('RGB', (max_width, max_heigth), (255, 255, 255))

    pat_diff_wid = int((max_width - patt.width)/2)
    pat_diff_hei = int((max_heigth - patt.height)/2)
    sce_diff_wid = int((max_width - scene.widthPxl)/2)
    sce_diff_hei = int((max_heigth - scene.heightPxl)/2)

    if (patt.width < scene.widthPxl) or (patt.height < scene.heightPxl):
        fina.paste(scen, (sce_diff_wid, sce_diff_hei))
        fina.paste(patt, (pat_diff_wid, pat_diff_hei))
    else:
        fina.paste(patt, (pat_diff_wid, pat_diff_hei))
        fina.paste(scen, (sce_diff_wid, sce_diff_hei))


    while True:

        img = np.array(fina)[::-1, ::-1].copy()

        ## real world coordinates
        x, y = vm.xy[0], vm.xy[1]

        ## normalised based on scene size
        x = (x - scene.widthCm/100/2)/(scene.widthCm/100) + 1
        y = (y - scene.heightCm/100/2)/(scene.heightCm/100) + 1

        # print x, y

        x = x*scene.widthPxl + sce_diff_wid
        y = (1-y)*scene.heightPxl + sce_diff_hei

        touchpoint = np.float32([[x, y]]).reshape(-1,1,2)
        cv2.circle(img, tuple(touchpoint.reshape(2)), 2, (0,0,0), 30)
        cv2.imshow('', img) #cv2.cvtColor(img, cv2.COLOR_GRAY2BGR))

        k = cv2.waitKey(1)
        key = chr(k & 255)
        if key == 'q':
            vm.stop()
            break

        if key == 'd':
            import ipdb; ipdb.set_trace()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Live tracking.')
    parser.add_argument('-p', metavar='pattern', help='optical pattern', required=True)
    parser.add_argument('-s', metavar='size', help='scene size', required=False, default='S4')
    args = parser.parse_args()

    main(args.p, args.s)
