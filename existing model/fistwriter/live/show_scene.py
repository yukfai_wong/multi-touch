import numpy as np
import cv2
import pyrealsense as pyrs
import time
import argparse

from sys import path
path.insert(1,'/Users/antoine/Documents/owndev/fistwriter_project/')
from fistwriter.process import spaces
from fistwriter.utils import ProcessingError


def main(scene, update=True):
    name = "scene with margins"
    cv2.namedWindow(name, 1)
    update_once = False

    while True:
        scene.camera.wait_for_frames()

        image = cv2.cvtColor(scene.camera.cad, cv2.COLOR_RGB2BGR)

        if update or update_once:
            update_once = False

            ## find homography
            try:scene.pattern.find_homography(image)
            except ProcessingError as e: continue
            try: scene.system.find_model(scene.camera.pointcloud)
            except ProcessingError as e: continue
            # pattern.find_homography(image)

            ## if pattern found, draw the surface
            if scene.pattern.mask is not None:
                imgDraw0 = scene.pattern.draw_contours(image)

            imgDraw1 = scene.draw_surface(image)
            imgDraw1 = scene.draw_surface_margins(image)

        else:
            imgDraw1 = image

        cv2.imshow(name, imgDraw1)

        # Check key input
        k = cv2.waitKey(1)
        key = chr(k & 255)
        if key == 'q':
            cv2.destroyWindow(name)
            cv2.waitKey(1)
            break
        if key == 'd':
            import ipdb; ipdb.set_trace()
        if key == 'u':
            update_once = True

    # cv2.destroyWindow(name)
    # cv2.waitKey(1)

    return scene



if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Live tracking.')
    parser.add_argument('-p', metavar='pattern', help='optical pattern', required=True)
    parser.add_argument('-s', metavar='size', help='scene size', required=False, default='S4')
    parser.add_argument('-l', help='turn off live tracking', action='store_false')
    args = parser.parse_args()

    import pyrealsense as pyrs
    pyrs.start()

    from pyrealsense.constants import rs_ivcam_preset, rs_option
    dev = pyrs.Device(ivcam_preset = rs_ivcam_preset.RS_IVCAM_PRESET_GESTURE_RECOGNITION)
    # dev.set_device_option(rs_option.RS_OPTION_SR300_AUTO_RANGE_UPPER_THRESHOLD, 4500)
    time.sleep(15)
    scene = spaces.Scene(dev, args.p, shape = args.s)

    dev.set_device_option(rs_option.RS_OPTION_F200_FILTER_OPTION, 2)
    dev.set_device_option(rs_option.RS_OPTION_SR300_AUTO_RANGE_UPPER_THRESHOLD, 3500)


    main(scene, args.l)

