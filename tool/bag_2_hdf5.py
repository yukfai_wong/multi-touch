import sys
from pathlib import Path
import pyrealsense2 as rs
import numpy as np
import h5py

# Configure depth and color streams
def main(file):
    config = rs.config()
    config.enable_device_from_file(file);
    print("config")
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    print("enable_depth_stream")
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
    print("enable_color_stream")
    config.enable_stream(rs.stream.infrared, 640, 480, rs.format.y8, 30)
    print("enable_IR_stream")
    pipeline = rs.pipeline()
    print("pipeline")
    # Start streaming
    pipeline.start(config)
    print("start")
    recording_file = h5py.File(file+".hdf5", 'a')
    recording_file.create_dataset(name="color", shape=(0,480,640,3), maxshape=(None,480,640,3), dtype=np.uint8, chunks=(1,480,640,3), compression="gzip", compression_opts=4)
    recording_file.create_dataset(name="depth", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint16, chunks=(1,480,640), compression="gzip", compression_opts=4)
    recording_file.create_dataset(name="IR", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint8, chunks=(1,480,640), compression="gzip", compression_opts=4)
    frame_num=-1
    first_frame = np.asanyarray(pipeline.wait_for_frames().get_color_frame().get_data())#used to check it is repeat
    try:
        while True:
            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()
            infrared_frame = frames.get_infrared_frame()

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())
            IR_image = np.asanyarray(infrared_frame.get_data())
            frame_num+=1
            new_shape = frame_num +1
            recording_file['color'].resize(new_shape, axis=0)
            recording_file['depth'].resize(new_shape, axis=0)
            recording_file['IR'].resize(new_shape, axis=0)
            recording_file['color'][frame_num]=color_image
            recording_file['depth'][frame_num]=depth_image
            recording_file['IR'][frame_num]=IR_image
            if(np.array_equal(first_frame, color_image)):
                recording_file.close()
                print(frame_num)
                break
    finally:
        # Stop streaming
        pipeline.stop()

if __name__ == "__main__":
    # execute only if run as a script
    if(len(sys.argv)<2):
        print("enter path to file")
    elif(Path(sys.argv[1]).is_file()):
        main(sys.argv[1])
    else:
        print("File do not exist")
