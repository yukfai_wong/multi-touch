import sys
from pathlib import Path

def main(file):
    from keras.models import load_model
    from homography import pyrs12, spaces
    import time
    import cv2
    import h5py
    import datetime
    import numpy as np
    import deproject
    import HandShapeAnalysis
    print("main")
    # Configure depth and color streams
    h5f = h5py.File(file, 'r')
    color_frame = h5f['color']
    depth_frame = h5f['depth']
    pcl_center = h5f['pcl_center'][...]
    trans_matrix = h5f['trans_matrix'][...]

    Total_frame = h5f['color'].shape[0]
    print("Total_frame: %d" % Total_frame)
    cv2.namedWindow('Bird\'s-eyes angle', cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow('camera angle', cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow('point image', cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow('Color', cv2.WINDOW_AUTOSIZE)

    #analyser = HandShapeAnalysis.Depth2Finger(pcl_center, trans_matrix)
    # analyser.set_show(debug = False,
    # finger_point_image = True, hull_image = False, detected_defect = True, cluster_point = False, possibility_modal = False)

    analyser2 = HandShapeAnalysis.Depth2Finger(pcl_center, trans_matrix)
    analyser2.set_show(debug=False,
                       finger_point_image=True, hull_image=False, detected_defect=True, cluster_point=False, possibility_modal=False)
    analyser2.RANSAC = True
    print(1)
    model = load_model('new_touch_model.h5')

    close_lock = False
    while True:
        print(1)
        for i in range(Total_frame):
            print(i)
            # Convert images to numpy arrays
            color_image = color_frame[i]

            # analyser.renew_high(depth_frame[i])
            areas, points = analyser2.renew_both(depth_frame[i])
            if(len(areas) == 0):
                high_angle = np.hstack((analyser2.H_finger_point_image, analyser2.H_detected_defect))
                camera_angle = np.hstack((analyser2.D_finger_point_image, analyser2.D_detected_defect))
                cv2.imshow('Color', cv2.resize(color_image, (0, 0), fx=0.5, fy=0.5))
                cv2.imshow('Bird\'s-eyes angle', high_angle)
                cv2.imshow('camera angle', camera_angle)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    close_lock = True
                    break
                continue

            x_predicts = analyser2.predict_return(areas)
            y_predict = model.predict(x_predicts)
            zip_sturcture = np.concatenate(
                (y_predict, analyser2.relative_coordinate), axis=1)

            for r in zip_sturcture:
                if (r[0] >= 0.9):  # if touching change color
                    p = np.concatenate(((r[1]*350).reshape(-1,1),
                     (r[2]*400).reshape(-1,1)),axis = 1)
                    cv2.drawContours(analyser2.finger_point_image, p.reshape(
                        -1, 1, 2).astype(int), -1, (0, 0, 255), 10)

            high_angle = np.hstack((analyser2.H_finger_point_image, analyser2.H_detected_defect))
            camera_angle = np.hstack((analyser2.D_finger_point_image, analyser2.D_detected_defect))
            finger_point_image = analyser2.finger_point_image
            finger_point_image = cv2.resize(finger_point_image, (0, 0), fx=0.8, fy=0.8)

            cv2.imshow('Color', cv2.resize(color_image, (0, 0), fx=0.5, fy=0.5))
            cv2.imshow('Bird\'s-eyes angle', high_angle)
            cv2.imshow('camera angle', camera_angle)
            cv2.imshow('point image', finger_point_image)
            cv2.waitKey(1)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                close_lock = True
                break
        if close_lock:
            break
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # execute only if run as a script
    if(len(sys.argv) < 2):
        print("enter path to file")
    elif(Path(sys.argv[1]).is_file()):
        main(sys.argv[1])
    else:
        print("File do not exist")
