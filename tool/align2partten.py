from homography import pyrs12, spaces
import cv2



cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
try:
    cam = pyrs12.Camera()
    cam.start()
    print("wasting first 30 loop, please don't move the camera")
    for i in range(30):
        cam.wait_for_frames()
        print(i,". ", end="", flush=True)
    print()
    while True:
        cam.wait_for_frames()
        scene = spaces.Scene(cam, './homography/rgb-marker.png')
        # Show images
        cv2.imshow('RealSense', scene.draw_surface(cam.cad))
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
finally:
    # Stop streaming
    cam.stop()
