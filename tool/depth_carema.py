import pyrealsense2 as rs
import numpy as np
import cv2

# Configure depth and color streams
pipeline = rs.pipeline()
print("pipeline")
config = rs.config()
print("config")
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
print("enable_depth_stream")
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
print("enable_color_stream")
config.enable_stream(rs.stream.infrared, 640, 480, rs.format.y8, 30)
print("enable_IR_stream")

# Start streaming
pipeline.start(config)
print("start")

try:
    while True:

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()
        infrared_frame = frames.get_infrared_frame()
        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())
        IR_image = np.asanyarray(infrared_frame.get_data())
        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
        draw_gray_image = cv2.cvtColor(IR_image, cv2.COLOR_GRAY2BGR)

        # Stack both images horizontally
        images = np.hstack((color_image, depth_colormap, draw_gray_image))
        images = cv2.resize(images, (0,0), fx=0.5, fy=0.5)
        # Show images
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', images)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
except Exception:
    pipeline.stop()

finally:

    # Stop streaming
    pipeline.stop()
