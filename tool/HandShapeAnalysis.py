##only python 3.5+
import numpy as np
import cv2, h5py, deproject
#import matplotlib.pyplot as plt #this cause crash in windows without error
from scipy.spatial import ckdtree
from deproject import random_sample
import sys
if sys.version_info[0] < 3 or (sys.version_info[0] == 3 and sys.version_info[1] < 5):
    raise Exception("Must be Python 3.5+")

class Depth2Finger():
    def __init__(self, pcl_center, trans_matrix):
        self.pcl_center = pcl_center
        self.trans_matrix = trans_matrix
        self.angle = 60
        self.cloud_min = 0.02
        self.cloud_max = 0.20

        self.depthimage = None

        self.show = shows(False, False, False, False, False, False)
        self.finger_point_image_coordinate = None
        self.finger_point_cloud_coordinate = None#only use it when either renew_high / renew_depth, not renew
        self.cloud = None
        self.cloud_filter = None
        self.cloud_image = None
        self.finger_point_image = None
        self.hull_image = None
        self.detected_defect = None
        self.cluster_point = None
        self.perviou_point = None
        self.process_image = None
        self.RANSAC = False
        self.RANSAC_parameter = None
        self.RANSAC_iterations = 100
        self.RANSAC_threshold = 3e-3
        self.RANSAC_min_required = 100
        self.polygonal = None


    def set_angle(self, angle):
        self.angle = angle
    def set_cloud_min(self, cloud_min):
        self.cloud_min = cloud_min
    def set_cloud_max(self, cloud_max):
        self.cloud_max = cloud_max
    def set_show(self, debug, finger_point_image, hull_image, detected_defect, cluster_point, possibility_modal):
        self.show = shows(debug, finger_point_image, hull_image, detected_defect, cluster_point, possibility_modal)

    def predict_ball(self, points):
        job_result = []

        tree = ckdtree.cKDTree(self.cloud)
        for tip_point in points:
            tip_index = tree.query_ball_point(tip_point, r=0.02)
            hist, bins = np.histogram(self.cloud[tip_index][:,2],
                              bins=20,
                              range=(-0.01, 0.06),
                              density=1)
            job_result.append(hist)
        return np.array(job_result)

    def predict_return(self, roi):
        job_result = []
        for tip_area in roi:
            tip_area = tip_area.reshape(-1,3)
            # z_axis = np.where(tip_area[:,2]>=0.06, 0.06, tip_area[:,2])
            z_axis = tip_area[:,2]
            hist, bins = np.histogram(z_axis,
                              bins=20,
                              range=(-0.01, 0.06),
                              density=1)
            job_result.append(hist)#/len(z_axis))
        return np.array(job_result)


    def renew_both(self, image):
        depth_area, depth_point= self.renew_depth(image)
        if self.show.finger_point_image:
            self.D_finger_point_image = np.copy(self.finger_point_image)
        if self.show.detected_defect:
            self.D_detected_defect = np.copy(self.detected_defect)
        hight_area, hight_point = self.renew_high(image)
        if self.show.finger_point_image:
            self.H_finger_point_image = np.copy(self.finger_point_image)
        if self.show.detected_defect:
            self.H_detected_defect = np.copy(self.detected_defect)

        if((len(hight_area)==0) and (len(depth_area)==0)):
            return np.array([]), np.array([])

        #if renew_high renew_depth return none, the checking process will broken, add if to avoid it.
        new_point = list()
        new_area = list()
        if(len(hight_area)!=0 and (len(depth_area)!=0)):
            for i in range(len(hight_point)):#use bird eye angle as a supplement to camera angle.
                #is_in_area check whether current hight point are in the square formed by depth_point[:,:2] - 0.02 and depth_point[:,:2] + 0.02
                #and sum the result and check whether there is 2 true, which mean it is close to the circle
                # is_in_area = np.sum(((depth_point[:,:2] - 0.02) <= hight_point[i][:2]) & (hight_point[i][:2] <= (depth_point[:,:2] + 0.02))
                #        ,axis = 1) == 2

                distance = np.linalg.norm(depth_point[:,:2] - hight_point[i][:2],axis = 1)
                minimun_idx = np.argmin(distance)
                is_in_area = distance[minimun_idx] < 0.02
                if self.show.debug:
                    print("point ",i,np.invert(is_in_area))
                if np.invert(is_in_area).all():#if the point is not in all of those area
                    new_point.append(hight_point[i])
                    new_area.append(hight_area[i])

        if(len(hight_area)!=0 and (len(depth_area)==0)):
            new_point = hight_point
            new_area = hight_area

        self.finger_point_cloud_coordinate = np.array([*depth_point, *new_point])

        #cover point to image data
        self.relative_coordinate = np.empty(shape = self.finger_point_cloud_coordinate.shape)

        self.relative_coordinate[:,0] = self.finger_point_cloud_coordinate[:,0]-xmin_
        self.relative_coordinate[:,0] = self.relative_coordinate[:,0]/(xmax_-xmin_)
        self.relative_coordinate[:,1] = self.finger_point_cloud_coordinate[:,1]-ymin_
        self.relative_coordinate[:,1] = self.relative_coordinate[:,1]/(ymax_-ymin_)

        self.relative_coordinate[:,2] = self.finger_point_cloud_coordinate[:,2]


        if self.show.finger_point_image:
            point = np.concatenate(((self.relative_coordinate[:,0]*350).reshape(-1,1),
             (self.relative_coordinate[:,1]*400).reshape(-1,1)),axis = 1)
            point = np.floor(point).astype(np.int32)
            self.finger_point_image = np.zeros(shape = (350,400,3),dtype = np.uint8)
            cv2.drawContours(self.finger_point_image, point.reshape(-1,1,2), -1, (255,0,0), 20)

        if self.show.debug:
            print(np.array([*depth_point, *new_point]))
            print("----------renew_both---------")
        return np.array([*depth_area, *new_area]), np.array([*depth_point, *new_point])

    def renew_high(self, image):
        self.depthimage = image
        mat = self.trans_matrix
        org = self.pcl_center

        CLOUD = depth2cloud(image, mat, org)
        self.cloud = CLOUD.reshape((480, 640, 3))

        CLOUD = CLOUD.reshape((480, 640, 3))[image != 0].reshape((-1,3))
        roi, ratio= ROI_filter(CLOUD)

        intrease_area = CLOUD[roi]

        if(self.RANSAC):
            intrease_area = CLOUD[roi]
            intrease_area_non_zero = np.where(intrease_area[:,2]>=0, intrease_area[:,2],np.zeros(shape=intrease_area.shape[0:-1]))
            intrease_area[:,2] = intrease_area_non_zero
            #this function for avoid recreate a RANSAC model, however, doesn't seems to working because the float of point cloud to serious
            if(self.RANSAC_parameter is None):
                table_filter, center, normal = filter_by_ransac(intrease_area, iterations =100, threshold = self.RANSAC_threshold, min_required = self.RANSAC_min_required)
                # self.RANSAC_parameter = (center, normal)
            else:
                center, normal = self.RANSAC_parameter
                vectors = np.subtract(intrease_area, center)
                distances = np.abs(np.dot(vectors, normal))
                inliers = distances <= self.RANSAC_threshold
                table_filter = np.invert(inliers)
        else:
            table_filter = filter_by_min(intrease_area,self.cloud_min)

        filtered = intrease_area[table_filter]

        self.cloud_filter = np.copy(filtered).reshape(-1,3)

        #tranform the filtered point cloud to
        bird_eyes_image, cloud_image = cloud2bird_image(filtered, np.min(intrease_area[:,1]), np.max(intrease_area[:,1]), np.min(intrease_area[:,0]), np.max(intrease_area[:,0]))
        self.cloud_image = np.copy(cloud_image)

        self.realimage = np.copy(bird_eyes_image)
        self.Depth2FingerPoints(denoising(np.copy(bird_eyes_image)), polygonal = self.polygonal, angle = self.angle, show = self.show)

        new_image_coordinate = list()
        cloud_image_available_point_mask = bird_eyes_image>0
        for p in self.finger_point_image_coordinate:##fix the point that return (0,0,0)
            if not (cloud_image_available_point_mask[p[1], p[0]]):
                p = search_closest_nonzero(p, cloud_image_available_point_mask)
            if not (p is None):
                new_image_coordinate.append(p)
        self.finger_point_image_coordinate = new_image_coordinate
        if self.show.finger_point_image:
            cv2.drawContours(self.finger_point_image, np.array(self.finger_point_image_coordinate).reshape(len(self.finger_point_image_coordinate),1,2), -1, (255,0,0), 10)


        area_image = []
        cloud_point = []
        for p in self.finger_point_image_coordinate:
            distance = np.linalg.norm(self.cloud.reshape(-1,3) - cloud_image[p[1], p[0]],axis = 1)
            circling = self.cloud .reshape(-1,3)[distance < 0.02]

            area_image.append(circling)
            cloud_point.append(cloud_image[p[1], p[0]][...,[0,1,2]])
        self.finger_point_cloud_coordinate = np.array(cloud_point)
        if self.show.debug:
            print("high_point")
            print(self.finger_point_image_coordinate)
            print(np.array(cloud_point))
            print("----------renew_high---------")
        return np.array(area_image), np.array(cloud_point)


    def renew_depth(self, image):
        self.depthimage = image
        org = self.pcl_center
        mat = self.trans_matrix

        CLOUD = depth2cloud(image, mat, org).reshape((480, 640, 3))
        #homography process
        self.cloud = np.copy(CLOUD)

        roi, _ = ROI_filter(CLOUD)
        if(self.RANSAC):
            roi_cloud = CLOUD[roi]
            ROI_z_non_zero = np.where(roi_cloud[:,2]>0, roi_cloud[:,2],np.zeros(shape=roi_cloud[:,2].shape))
            roi_cloud[:,2] = ROI_z_non_zero
            #this function for avoid recreate a RANSAC model, however, doesn't seems to working because the float of point cloud to serious
            if(self.RANSAC_parameter is None):
                roi[roi==True], center, normal = filter_by_ransac(roi_cloud, iterations =100, threshold = self.RANSAC_threshold, min_required = self.RANSAC_min_required)#overwrite
                # self.RANSAC_parameter = (center, normal)
            else:##This should save time
                center, normal = self.RANSAC_parameter
                vectors = np.subtract(roi_cloud, center)
                distances = np.abs(np.dot(vectors, normal))
                inliers = distances <= self.RANSAC_threshold
                roi[roi==True] = np.invert(inliers)
            mask_filter = roi
        else:
            table_filter = filter_by_min(CLOUD,self.cloud_min)
            mask_filter = roi & table_filter


        ##mask_filter = (in ROI area & z-axis in limit)
        mask_filter = mask_filter.reshape(CLOUD.shape[0],CLOUD.shape[1])
        self.cloud_filter = np.copy(CLOUD[mask_filter])
        camera_angle_grey_image, depth_image_CLOUD = CLOUD2camera_projection(CLOUD, mask_filter, np.min(self.cloud_filter[:,2]), np.max(self.cloud_filter[:,2]))
        self.cloud_image = np.copy(depth_image_CLOUD)

        self.realimage = np.copy(camera_angle_grey_image)
        self.Depth2FingerPoints(denoising(camera_angle_grey_image), polygonal = self.polygonal, angle = self.angle, show = self.show)

        new_image_coordinate = []
        for p in self.finger_point_image_coordinate:##fix the point that return (0,0,0)
            if not (mask_filter[p[1], p[0]]):
                p = search_closest_nonzero(p, mask_filter)
            if not (p is None):
                new_image_coordinate.append(p)
        self.finger_point_image_coordinate = new_image_coordinate


        if self.show.finger_point_image:
            cv2.drawContours(self.finger_point_image, np.array(self.finger_point_image_coordinate).reshape(len(self.finger_point_image_coordinate),1,2), -1, (255,0,0), 10)

        ##draw region
        area_image = []
        cloud_point = []
        for p in self.finger_point_image_coordinate:
            distance = np.linalg.norm(self.cloud.reshape(-1,3) - depth_image_CLOUD[p[1], p[0]],axis = 1)
            circling = self.cloud.reshape(-1,3)[distance < 0.02]

            area_image.append(circling)
            cloud_point.append(depth_image_CLOUD[p[1], p[0]])
        self.finger_point_cloud_coordinate = np.array(cloud_point)

        if self.show.debug:
            print("new_image_coordinate")
            print(np.array(new_image_coordinate))
            print("depth_point")
            print(np.array(cloud_point))
            print("----------renew_depth---------")
        return np.array(area_image), np.array(cloud_point)

    def Depth2FingerPoints(self, process_image, angle =60, polygonal = None,  show = (False,False,False,False,False)):
        self.process_image = np.copy(process_image)
        colored = cv2.cvtColor(process_image, cv2.COLOR_GRAY2RGB)
        _,rocess_image = cv2.threshold(process_image,10,255,cv2.THRESH_BINARY)

        if show.finger_point_image:
            self.finger_point_image = np.copy(colored)
        if show.hull_image:
            self.polygonal_image = np.zeros(colored.shape)
            self.Contours = np.copy(colored)
            self.hull_image = np.copy(colored)
        if show.detected_defect:
            self.detected_defect = np.copy(colored)
        if show.cluster_point:
            self.cluster_point = np.copy(colored)
        if show.possibility_modal:
            self.possibility_modal = np.copy(colored)

        _, contours, _=cv2.findContours(process_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        if(len(contours)==0):
            if show.debug:
                print("Error, no contours found")
            self.finger_point_image_coordinate = np.array([])
            return self

        contours = max(contours, key=lambda x: cv2.contourArea(x))
        if(len(contours)<2):
            if show.debug:
                print("Error, not enough contours to form a Hull")
            self.finger_point_image_coordinate = np.array([])
            return self
        if(len(contours)<3):
            if show.debug:
                print("Error, not enough contours to form a defect")
            self.finger_point_image_coordinate = np.array([])
            return self

        if not (polygonal is None):
            self.polygonal_image, contours = draw_polygonal(self.polygonal_image, contours)

        hull=cv2.convexHull(contours,returnPoints=True)
        hull_point = hull.reshape(len(hull),2)
        if show.hull_image:
            self.Contours = cv2.drawContours(self.Contours, contours, -1, (0,255,0), 3)
            self.hull_image = cv2.drawContours(self.hull_image, hull, -1, (255,0,0), 10)

        ##find the point that too close to each other
        clusted_point = clustering_by_distance(hull_point)

        c = contours.reshape(-1,2)
        cloest_to_mean_cluster = []
        for i in clusted_point:
            minIndex, minValue = min(enumerate(c), key=lambda v: np.linalg.norm(i - v[1]))
            cloest_to_mean_cluster.append(minIndex)
        clusted_indexing = np.asarray(cloest_to_mean_cluster)
        if show.debug:
            print("clusted_point")
            print(clusted_point)
        if show.cluster_point:
            cv2.drawContours(self.cluster_point, clusted_point.reshape(len(clusted_point),1,2), -1, (255,0,0), 10)


        defects = cv2.convexityDefects(contours, clusted_indexing)

        if(defects is None or len(defects)<2):
            if show.debug:
                print("Error, not enough defects")
            # self.perviou_point = np.copy(self.finger_point_image_coordinate)
            self.finger_point_image_coordinate = np.array([])
            return self

        if show.detected_defect:
            for i in range(defects.shape[0]):
                s,e,f,d = defects[i,0]
                start = tuple(contours[s][0])
                end = tuple(contours[e][0])
                far = tuple(contours[f][0])
                # if show.debug:
                #     print("start: ",start)
                #     print("End:   ", end)
                #     print("Defect:", far)
                cv2.line(self.detected_defect,start,end,[0,255,0],2)
                cv2.line(self.detected_defect,start,far,[0,255,255],2)
                cv2.line(self.detected_defect,end,far,[0,255,255],2)
                cv2.circle(self.detected_defect,start,5,[0,255,0],2)
                cv2.circle(self.detected_defect,end,5,[0,0,255],2)
                cv2.circle(self.detected_defect,far,5,[255,255,255],2)


        hall_index = np.concatenate((defects[:,0,0],defects[:,0,1]))##all the hall that start and end a defect
        maping_hall = dict()
        for i in hall_index:
            maping_hall[i] = []
        for i in range(defects.shape[0]):
            s,e,f,_ = defects[i,0]
            maping_hall[s].append(f)
            maping_hall[e].append(f)
        key = list(maping_hall.keys())
        for k in key:
            if(len(maping_hall[k])<2):
                del maping_hall[k]
        dd_vector = np.array(list(maping_hall.values()))##list of defects
        hall_vector = np.array(list(maping_hall.keys()))##list of hall
        dd_point = contours[dd_vector][:,:,0]
        hall_point = contours[hall_vector][:,0]

        ##apply cosine law  cos(A) = (b^2+c^2-a^2)/(2bc)
        Ap = hall_point
        Bp = dd_point[:,0]
        Cp = dd_point[:,1]
        ##sqrt((p1-p2)**2)
        a_distance = np.sum(np.power(Bp-Cp,2),axis =1)
        b_distance = np.sum(np.power(Ap-Cp,2),axis =1)
        c_distance = np.sum(np.power(Ap-Bp,2),axis =1)
        Angle_A = np.arccos((b_distance + c_distance - a_distance)/(2 * np.sqrt(b_distance) * np.sqrt(c_distance))) * (180 / np.pi)
        isfinger = Angle_A < angle
        matched = hall_point[isfinger]
        matched = point_filter(matched, process_image.shape)
        self.finger_point_image_coordinate = matched
        if show.debug:
            print("Angle_A")
            print(Angle_A)
            print("matched")
            print(matched)
        #
        # if show.possibility_modal & (not self.perviou_point is None):
        #     if(len(self.perviou_point)>1):
        #         possitive_point = []
        #         for i in self.finger_point:
        #             if np.sum(distance(i,self.perviou_point))!=0:
        #                 possitive_point.append(i)
        #         possitive_point = np.asarray(possitive_point)
        #         cv2.drawContours(self.possibility_modal, possitive_point.reshape(len(possitive_point),1,2), -1, (0,0,255), 10)

def incloud(bondary, x, y, area = 20):
    return ((x+area)<bondary[1]) & ((x-area)>0) & ((y+area)<bondary[0]) & ((y-area)>0)

class shows:
    def __init__(self, debug, finger_point_image, hull_image, detected_defect, cluster_point, possibility_modal):
        self.debug = debug
        self.finger_point_image = finger_point_image
        self.hull_image = hull_image
        self.detected_defect = detected_defect
        self.cluster_point = detected_defect
        self.possibility_modal = possibility_modal

def depth2cloud(image, mat, org, show = False):
    cloud = deproject.compute(image.astype(np.short()))
    point_original = (np.dot(mat, (cloud - org).reshape((-1,3)).T).T)
    # if show: plt.imshow(point[image != 0].reshape((-1,3)))
    return point_original#, point[without_table].reshape((-1,3))


def draw_polygonal(polygonal_image, contours):
    contours = cv2.approxPolyDP(curve = contours, epsilon = 8, closed = True);
    pervious = None
    for i in contours:
        if pervious is None:
            pervious = i
            continue
        cv2.line(polygonal_image,tuple(pervious[0]),tuple(i[0]),[0,255,0],3)
        pervious = i
    cv2.line(polygonal_image,tuple(contours[0][0]),tuple(contours[-1][0]),[0,255,0],3)
    _, contours, _=cv2.findContours(cv2.cvtColor(polygonal_image.astype(np.uint8), cv2.COLOR_BGR2GRAY), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = max(contours, key=lambda x: cv2.contourArea(x))
    return polygonal_image, contours

def CLOUD2camera_projection(CLOUD,mask_filter, min_, max_):
    CLOUD = np.reshape(CLOUD,(480,640,3))
    mask_filter = mask_filter.reshape(CLOUD.shape[0],CLOUD.shape[1],1)
    depth_image_CLOUD = np.where(mask_filter, CLOUD, np.array([np.nan, np.nan, min_]))
    camera_angle_grey_image = (((depth_image_CLOUD[:,:,2]-min_)*255)/max_)
    return camera_angle_grey_image.astype(np.uint8), depth_image_CLOUD

def cloud2bird_image(cloud, min_x, max_x, min_y, max_y):
    cloud_cp = np.copy(cloud.reshape((-1,3)))
    #normalise x, y axis to given scale
    cloud_cp[:,1] = (cloud_cp[:,1]-min_x)/(max_x-min_x)
    cloud_cp[:,0] = (cloud_cp[:,0]-min_y)/(max_y-min_y)

    y_axis = 350
    ratio = ((max_x - min_x) / (max_y - min_y))#aspect ratio

    size = (y_axis, np.int(y_axis * ratio))#(y-axis, x-axis)
    core = np.array([[size[1]-10,0,0],[0,size[0]-10,0],[0,0,1]])
    twoD_coordinate = np.dot(cloud_cp, core)
    twoD_coordinate = np.concatenate((twoD_coordinate, cloud[:,:2]),axis = 1)
    #twoD_coordinate(image_x,image_y,point_z,point_x,point_y)
    twoD_coordinate = twoD_coordinate[np.argsort(twoD_coordinate[:,2])]
    #so that, sort by z-axis, higher point overwrite lower
    image = np.zeros((size[0], size[1],3))
    #strat with empty image
    image[twoD_coordinate[:,1].astype(np.int),twoD_coordinate[:,0].astype(np.int)] = twoD_coordinate[:,2:][...,[1,2,0]]
    #full 3D point to 2D image
    # image = image[:,::-1]##point cloud left x increase, however image reduce to 0, so [:,1] to show

    cloud_coordinate = np.copy(image)

    #extract z-axis
    bird_eyes_image = image[:,:,2]
    #normalize pointcloud z axis to image data
    bird_eyes_image = ((bird_eyes_image-np.min(bird_eyes_image))*255)/np.max(bird_eyes_image)
    return bird_eyes_image.astype(np.uint8), cloud_coordinate


def clustering_by_distance(remain_point, threshold=20):
    all_cluster = []
    remain_point = remain_point.tolist()
    while(len(remain_point) != 0):
        first = remain_point.pop(0)
        remain_point, cluster = check_closePoint_list(remain_point, [first])
        all_cluster.append(cluster.tolist())
    mean_per_cluster = []
    for cluster in all_cluster:
        mean_per_cluster.append(np.mean(np.array(cluster),axis =0).astype(np.int32))
    return np.asarray(mean_per_cluster)

##For all the point in cluster whether there is points that distance smaller than
def check_closePoint_list(points, cluster, threshold=20):
    have_close = False
    new_cluster = np.empty((0,2),dtype = np.int32)
    if(len(points)==0):##There is nothing to check if given points is empty, just return it
        return points, np.asarray(cluster)
    for p in cluster:
        dist = distance(np.asarray(p), np.asarray(points), threshold=threshold)
        have_close = np.sum(dist) > 0##check if there is new point added in
        new_cluster = np.concatenate((new_cluster, np.asarray(points)[dist]),axis = 0)
        points = np.asarray(points)[np.logical_not(dist)].tolist()#delete the cludtered point
        if(len(points)==0):#not more to merge
            return [], new_cluster
    if have_close:##if there is new point added in, need to check if new point link to other point
        return check_closePoint_list(points,new_cluster)
    else:
        return points, np.concatenate((cluster,new_cluster),axis = 0)

def distance(p1, p2, threshold=20):
    p1 = np.array(p1)
    p2 = np.array(p2)
    if(len(p2.shape) == 1):
        result = np.sum(np.power(p2-p1,2),axis = 0)
    else:
        result = np.sum(np.power(p2-p1,2),axis = 1)
    return  result < threshold*threshold


heightCm = 16.93
widthCm = 25.4

xmin = - widthCm / 100 / 2
xmax = widthCm / 100 / 2
ymin = - heightCm / 100 / 2
ymax = heightCm / 100 / 2

## add 5cm margin
margin = 0.05
xmin_ = xmin - margin
xmax_ = xmax + margin
ymin_ = ymin - 0.1##give the clear hand image when finger touching the edge
ymax_ = ymax + margin
def ROI_filter(cloud):
    xm = (cloud[...,1] >= xmin_) & (cloud[...,1] <= xmax_)
    ym = (cloud[...,0] >= ymin_) & (cloud[...,0] <= ymax_)
    ratio = ((xmax_ - xmin_)/(ymax_ - ymin_))

    return (xm & ym), ratio


def denoising(image, kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))):
    closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel)
    return opening

def point_filter(point, bounds):#bounds = (y, x)
    bound_y = ((point[:,0] - 10) > 0) & ((point[:,0] + 30) < bounds[0])
    bound_x = ((point[:,1] - 10) > 0) & ((point[:,1] + 10) < bounds[1])
    return point[bound_y & bound_x]

def search_closest_nonzero(point, mask):##mask: if the point is not use able mark as False
    loop = 1
    while loop<10:
        topleftx      = point[0] - loop
        toplefty      = point[1] - loop
        toprightx     = point[0] + loop
        toprighty     = point[1] - loop
        bottomleftx   = point[0] + loop
        bottomlefty   = point[1] - loop
        bottomrightx  = point[0] + loop
        bottomrighty  = point[1] + loop
        #oxx
        #oso
        #ooo
        if((topleftx<0)|(toprightx==mask.shape[1])|(toplefty<0)):
            break
        for i in range(topleftx+1,toprightx,1):
            if mask[toplefty, i]:
                return np.array([i,toplefty])
        #ooo
        #osx
        #oox
        if((toprighty<0)|(bottomrighty==mask.shape[0])|(toprightx==mask.shape[1])):
            break
        for i in range(toprighty+1,bottomrighty,1):
            if mask[i, toprightx]:
                return np.array([toprightx, i])
        #ooo
        #oso
        #xxo
        if((bottomrightx==mask.shape[1])|(bottomleftx<0)|(bottomrighty==mask.shape[0])):
            break
        for i in range(bottomleftx, bottomrightx-1,1):
                return np.array([i, bottomrighty])
        #xoo
        #xso
        #ooo
        if((bottomlefty==mask.shape[0])|(toplefty<0)|(bottomleftx<0)):
            break
        for i in range(toplefty, bottomlefty-1,1):
            if mask[i, bottomleftx]:
                return np.array([bottomleftx,i])
        loop+=1
    return None


def filter_by_min(points, cloud_min):
    shape = points.shape[0:-1]
    return (points.reshape(-1,3)[:,2]>cloud_min).reshape(shape)

def filter_by_ransac(points, iterations=100, threshold=3e-3, min_required=100):
    i = 0
    length = len(points)

    while i < iterations:
        index = random_sample(length-1, 3)
        n_points = points[index]

        center = n_points[0]
        centre_point1_vector = np.subtract(n_points[1], center)
        centre_point2_vector = np.subtract(n_points[2], center)
        normal_vector = np.cross(centre_point1_vector, centre_point2_vector)

        normal_vector_normalised = np.divide(normal_vector, np.linalg.norm(normal_vector))

        vectors = np.subtract(points, center)
        distances = np.abs(np.dot(vectors, normal_vector_normalised))

        inliers = distances <= threshold
        n_inliers = np.sum(inliers)

        if n_inliers > min_required:
            best_inliers = inliers
            best_normal = np.copy(normal_vector_normalised)
            best_center = np.copy(center)
            min_required = n_inliers
        i+=1
    if best_inliers is None:
        raise ValueError("did not meet fit acceptance criteria")
    return [np.invert(best_inliers), best_center, best_normal]


def draw_on_depth(depth, point):
    return cv2.drawContours(cv2.cvtColor(depth, cv2.COLOR_GRAY2RGB), point.reshape(len(point),1,2), -1, (0,0,255), 20)

def viewer(point):
    import pptk
    v = pptk.viewer(point)
    v.play([[0, 0, 0, 1 * np.pi/2, np.pi/4, 0.5]])
