import pyrealsense2 as rs
import numpy as np
import cv2
import datetime

# Configure depth and color streams

def main():
    config = rs.config()
    now=datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    config.enable_record_to_file("Vpyrealsense2_"+now+".bag");
    print("Save into file Vpyrealsense2_"+now+".bag")
    print("config")
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    print("enable_depth_stream")
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
    print("enable_color_stream")
    config.enable_stream(rs.stream.infrared, 640, 480, rs.format.y8, 30)
    print("enable_IR_stream")

    pipeline = rs.pipeline()
    print("pipeline")
    # Start streaming
    pipeline.start(config)
    print("start")

    try:
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        while True:
            FPS_start = datetime.datetime.now()
            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()
            infrared_frame = frames.get_infrared_frame()
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())
            IR_image = np.asanyarray(infrared_frame.get_data())
            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            # Stack both images horizontally
            images = np.hstack((color_image, depth_colormap))
            images = cv2.resize(images, (0,0),fx=0.5, fy=0.5)

            # Show images
            cv2.imshow('RealSense', images)
            #show_FPS
            FPS_end = datetime.datetime.now()
            print(cal_FPS(FPS_start, FPS_end), end='')
            print("\r", end='')
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    except RuntimeError:
        print("No more frames arrived, reached end of BAG file!")

    finally:
        # Stop streaming
        pipeline.stop()

def cal_FPS(start, end):
    diff = end-start
    return 1/(diff.seconds+(diff.microseconds*1e-06))

if __name__ == "__main__":
    # execute only if run as a script
    main()
