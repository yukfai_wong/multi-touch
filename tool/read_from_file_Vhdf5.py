import sys
from pathlib import Path
import numpy as np
import cv2
import h5py


def main(file):
    # Configure depth and color streams
    h5f = h5py.File(file, 'r')
    color_frame= h5f['color']
    depth_frame= h5f['depth']
    IR_frame= h5f['IR']

    Total_frame = h5f['color'].shape[0]
    print("Total_frame: %d"%Total_frame)

    cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
    for i in range(Total_frame):
        # Convert images to numpy arrays
        color_image = color_frame[i]
        IR_image = IR_frame[i]
        depth_image = depth_frame[i]

        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
        draw_gray_image = cv2.cvtColor(IR_image.astype(np.uint8), cv2.COLOR_GRAY2BGR)
        # Stack both images horizontally
        images = np.hstack((color_image, depth_colormap, draw_gray_image))
        print(color_image.dtype, depth_colormap.dtype, draw_gray_image.dtype)
        images = cv2.resize(images, (960, 240))
        cv2.imshow('RealSense', images.astype(np.uint8()))
        # Show images
        cv2.waitKey(0)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    # execute only if run as a script
    if(len(sys.argv)<2):
        print("enter path to file")
    elif(Path(sys.argv[1]).is_file()):
        main(sys.argv[1])
    else:
        print("File do not exist")
