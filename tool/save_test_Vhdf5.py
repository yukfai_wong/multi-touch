import pyrealsense2 as rs
import numpy as np
import cv2
import datetime
import h5py

recording_switch=False#too sure only 0->1 1->0 enable to start or stop recording
recording_file = None
frame_num = -1

def main():
    global recording_file
    global recording_switch
    # Configure depth and color streams
    config = rs.config()
    print("config")
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    print("enable_depth_stream")
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
    print("enable_color_stream")
    config.enable_stream(rs.stream.infrared, 640, 480, rs.format.y8, 30)
    print("enable_IR_stream")

    pipeline = rs.pipeline()
    print("pipeline")
    # Start streaming
    pipeline.start(config)
    print("start")
    cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
    cv2.createTrackbar('recording_switch 0=off','RealSense',0,1,recording)

    #recording config

    try:
        while True:
            FPS_start = datetime.datetime.now()
            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()
            infrared_frame = frames.get_infrared_frame()

            # Convert images to numpy arrays
            color_image = np.asanyarray(color_frame.get_data())
            depth_image = np.asanyarray(depth_frame.get_data())
            IR_image = np.asanyarray(infrared_frame.get_data())
            if recording_switch:
                global frame_num
                frame_num+=1
                new_shape = frame_num +1
                recording_file['color'].resize(new_shape, axis=0)
                recording_file['depth'].resize(new_shape, axis=0)
                recording_file['IR'].resize(new_shape, axis=0)
                recording_file['color'][frame_num]=color_image
                recording_file['depth'][frame_num]=depth_image
                recording_file['IR'][frame_num]=IR_image

            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
            draw_gray_image = cv2.cvtColor(IR_image, cv2.COLOR_GRAY2BGR)

            # Stack both images horizontally
            images = np.hstack((color_image, depth_colormap, draw_gray_image))
            images = cv2.resize(images, (960, 240))
            #show_FPS
            FPS_end = datetime.datetime.now()

            print(cal_FPS(FPS_start, FPS_end), end='')
            print("\r", end='')

            # Show images
            cv2.imshow('RealSense', images)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                if recording_switch:
                    recording_file.close()
                break
    finally:
        # Stop streaming
        pipeline.stop()

def recording(*args):
    global recording_file
    global recording_switch
    global frame_num
    bar_place = cv2.getTrackbarPos('recording_switch 0=off','RealSense')
    if(bar_place ==1):
        if not recording_switch:
            now=datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
            recording_file = h5py.File("Vhdf5_"+now+".hdf5", 'a')
            recording_file.create_dataset(name="color", shape=(0,480,640,3), maxshape=(None,480,640,3), dtype=np.uint8, chunks=(1,480,640,3), compression="gzip", compression_opts=4)
            recording_file.create_dataset(name="depth", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint16, chunks=(1,480,640), compression="gzip", compression_opts=4)
            recording_file.create_dataset(name="IR", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint8, chunks=(1,480,640), compression="gzip", compression_opts=4)
            frame_num=-1
            recording_switch = True
    else:
        if recording_switch:
            recording_switch = False
            recording_file.close()

def cal_FPS(start, end):
    diff = end-start
    return 1/(diff.seconds+(diff.microseconds*1e-06))

if __name__ == "__main__":
    # execute only if run as a script
    main()
