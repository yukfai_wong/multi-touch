import numpy as np
import cv2, h5py, deproject, datetime
from homography import pyrs12, spaces
import HandShapeAnalysis
import time

import concurrent.futures

#partten
pcl_center = None
trans_matrix = None

def main():
    global pcl_center, trans_matrix
    # Configure depth and color streams
    cam = pyrs12.Camera()
    cam.start()
    print("start")
    cv2.namedWindow('Color', cv2.WINDOW_AUTOSIZE)
    # cv2.namedWindow('Bird eyes view', cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow('depth angle', cv2.WINDOW_AUTOSIZE)
    #recording config

    try:
        print("wasting first 30 loop, please don't move the camera")
        for i in range(30):
            cam.wait_for_frames()
            print(i,". ", end="", flush=True)
        print()
        #partten
        get_partten(cam)
        while  np.isnan(trans_matrix[0][0]):
            get_partten(cam)
        print("partten recorded")
        print("pcl_center")
        print(pcl_center)
        print("trans_matrix")
        print(trans_matrix)

        #main recording
        analyser = HandShapeAnalysis.Depth2Finger(pcl_center, trans_matrix)
        analyser.set_show(debug = False, finger_point_image = True, hull_image = False, detected_defect = True, cluster_point = False, possibility_modal = False)
        analyser2 = HandShapeAnalysis.Depth2Finger(pcl_center, trans_matrix)
        analyser2.set_show(debug = False, finger_point_image = True, hull_image = False, detected_defect = True, cluster_point = False, possibility_modal = False)
        while True:
            FPS_start = datetime.datetime.now()
            # Wait for a coherent pair of frames: depth and color
            frames = cam.wait_for_frames()
            color_image, depth_image, IR_image = cam.cdi

            #analyser.renew_high(depth_frame[i])
            areas = analyser2.renew_depth(depth_image)
            if(len(areas) == 0):
                #high_angle = np.hstack((analyser.detected_defect, analyser.finger_point_image))
                camera_angle = np.hstack((analyser2.detected_defect, analyser2.finger_point_image))
                camera_angle = cv2.resize(camera_angle, (0,0), fx=0.8, fy=0.8)
                cv2.imshow('Color', color_image)
                cv2.imshow('depth angle', camera_angle)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
                continue

            x_predicts = analyser2.predict_return(areas)
            y_predict = model.predict(x_predicts)
            zip_sturcture = np.concatenate((y_predict, analyser2.finger_point_image_coordinate),axis = 1)

            for r in zip_sturcture:
                if (r[0] == 1):##if touching change color
                    cv2.drawContours(analyser2.finger_point_image, r[1:].reshape(1,1,2).astype(int), -1, (0,0,255), 10)


            #high_angle = np.hstack((analyser.detected_defect, analyser.finger_point_image))
            camera_angle = np.hstack((analyser2.detected_defect, analyser2.finger_point_image))
            camera_angle = cv2.resize(camera_angle, (0,0), fx=0.8, fy=0.8)


            cv2.imshow('Color', color_image)
            #cv2.imshow('Bird\'s-eyes angle', high_angle)
            cv2.imshow('depth angle', camera_angle)
            if ((cv2.waitKey(1) & 0xFF) == ord('q')) :
                break
    finally:
        # Stop streaming
        cam.stop()

def cal_FPS(start, end):
    diff = end-start
    return 1/(diff.seconds+(diff.microseconds*1e-06))

def get_partten(cam):
    global pcl_center, trans_matrix
    for i in range(10):
        cam.wait_for_frames()
    scene = spaces.Scene(cam, './homography/rgb-marker.png')
    pcl_center = scene.system.pcl_center
    trans_matrix = scene.system.trans_matrix



main()
