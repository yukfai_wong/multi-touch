import pyrealsense2 as rs
import numpy as np
import cv2, datetime, h5py
from homography import pyrs12, spaces

recording_switch=False#too sure only 0->1 1->0 enable to start or stop recording
recording_file = None
path = None
frame_num = -1

save_path = "../recording_data/"

#partten
pcl_center = None
trans_matrix = None

def main():
    global recording_file
    global recording_switch
    # Configure depth and color streams
    cam = pyrs12.Camera()
    cam.start()
    print("start")
    cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
    cv2.createTrackbar('recording_switch 0=off','RealSense',0,1,recording)
    #recording config

    try:
        print("wasting first 30 loop, please don't move the camera")
        for i in range(30):
            cam.wait_for_frames()
            print(i,". ", end="", flush=True)
        print()
        #partten
        get_partten(cam)
        while np.isnan(trans_matrix[0][0]):
            get_partten(cam)
        print("partten recorded")
        print("pcl_center")
        print(pcl_center)
        print("trans_matrix")
        print(trans_matrix)
        #main recording
        while True:
            FPS_start = datetime.datetime.now()
            # Wait for a coherent pair of frames: depth and color
            frames = cam.wait_for_frames()
            color_image, depth_image, IR_image = cam.cdi

            if recording_switch:
                global frame_num
                frame_num+=1
                new_shape = frame_num +1
                recording_file['color'].resize(new_shape, axis=0)
                recording_file['depth'].resize(new_shape, axis=0)
                recording_file['IR'].resize(new_shape, axis=0)
                recording_file['color'][frame_num]=color_image
                recording_file['depth'][frame_num]=depth_image
                recording_file['IR'][frame_num]=IR_image

            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
            draw_gray_image = cv2.cvtColor(IR_image, cv2.COLOR_GRAY2BGR)

            # Stack both images horizontally
            images = np.hstack((color_image, depth_colormap, draw_gray_image))
            images = cv2.resize(images, (960, 240))
            #show_FPS
            FPS_end = datetime.datetime.now()

            print(cal_FPS(FPS_start, FPS_end), end='')
            print("\r", end='')

            # Show images
            cv2.imshow('RealSense', images)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                if recording_switch:
                    recording_file.close()
                break
    finally:
        # Stop streaming
        cam.stop()


def get_partten(cam):
    global pcl_center, trans_matrix
    for i in range(10):
        cam.wait_for_frames()
    scene = spaces.Scene(cam, './homography/rgb-marker.png')
    pcl_center = scene.system.pcl_center
    trans_matrix = scene.system.trans_matrix



def recording(*args):
    global recording_file, recording_switch, frame_num, path
    bar_place = cv2.getTrackbarPos('recording_switch 0=off','RealSense')
    if(bar_place ==1):
        if not recording_switch:
            global pcl_center, trans_matrix
            now=datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
            path = save_path+"Vhdf5_"+now+".hdf5"
            recording_file = h5py.File(path, 'a')
            recording_file.create_dataset(name="color", shape=(0,480,640,3), maxshape=(None,480,640,3), dtype=np.uint8, chunks=(1,480,640,3), compression="gzip", compression_opts=4)
            recording_file.create_dataset(name="depth", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint16, chunks=(1,480,640), compression="gzip", compression_opts=4)
            recording_file.create_dataset(name="IR", shape=(0,480,640), maxshape=(None,480,640), dtype=np.uint8, chunks=(1,480,640), compression="gzip", compression_opts=4)
            recording_file.create_dataset(name="pcl_center", shape=(3,), dtype=np.float32, chunks=(3,), data = pcl_center)
            recording_file.create_dataset(name="trans_matrix", shape=(3,3), dtype=np.float32, chunks=(3,3), data = trans_matrix)
            frame_num=-1
            recording_switch = True
    else:
        if recording_switch:
            recording_switch = False
            print(path, "saved")
            recording_file.close()

def cal_FPS(start, end):
    diff = end-start
    return 1/(diff.seconds+(diff.microseconds*1e-06))

if __name__ == "__main__":
    # execute only if run as a script
    main()
