import sys
from pathlib import Path
import numpy as np
import pyrealsense2 as rs
import cv2

# Configure depth and color streams

def main(file):
    config = rs.config()
    config.enable_device_from_file(file);
    print("config")
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    print("enable_depth_stream")
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
    print("enable_color_stream")
    config.enable_stream(rs.stream.infrared, 640, 480, rs.format.y8, 30)
    print("enable_IR_stream")
    pipeline = rs.pipeline()
    print("pipeline")
    # Start streaming
    pipeline.start(config)
    print("start")
    first_frame = np.asanyarray(pipeline.wait_for_frames().get_color_frame().get_data())#used to check it is repeat
    try:
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        i = 0
        while True:
            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()
            infrared_frame = frames.get_infrared_frame()

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())
            IR_image = np.asanyarray(infrared_frame.get_data())
            i+=1
            if(np.array_equal(first_frame, color_image)):
                print(i)
                break

            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            images = np.hstack((color_image, depth_colormap))

            # Show image
            cv2.imshow('RealSense', images)
            if cv2.waitKey(1) & 0xFF == ord('q') :
                break
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    finally:

        # Stop streaming
        pipeline.stop()

if __name__ == "__main__":
    # execute only if run as a script
    if(len(sys.argv)<2):
        print("enter path to file")
    elif(Path(sys.argv[1]).is_file()):
        main(sys.argv[1])
    else:
        print("File do not exist")
