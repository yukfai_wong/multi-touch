import numpy as np
cimport numpy as np
from libc.stdlib cimport rand, RAND_MAX
#
# import yaml
# with open('sr300_610205001689.param', 'r') as fh:
#     d = yaml.load(fh)
# intr_ = d['610205001689']
#
# # intr = rs.intrinsics()
# _coeffs = intr_['coeffs']
# _width  = intr_['width']
# _height = intr_['height']
# _ppx    = intr_['ppx']
# _ppy    = intr_['ppy']
# _fx     = intr_['fx']
# _fy     = intr_['fy']
# # _model  = intr_['model']
# _coeffs = intr_['coeffs']
# _ds     = intr_['depth_scale']


DTYPE = np.float32
ctypedef np.float32_t DTYPE_t

def compute(short[:,:]depth_image):
  cdef int width  = 640
  cdef int height = 480
  cdef float ppx  = 314.79681396484375
  cdef float ppy  = 245.8909912109375
  cdef float fx   = 475.529052734375
  cdef float fy   = 475.5289306640625
  cdef double depth_scale = 0.00012498664727900177
  cdef float c0  = 0.14429353177547455
  cdef float c1  = 0.05476438254117966
  cdef float c2  = 0.004519885405898094
  cdef float c3  = 0.002106300089508295
  cdef float c4  = 0.10783065110445023
  # intrinsics and depth_scale are global
  # cdef double depth_scale = _ds
  # cdef int height = _height
  # cdef int width  = _width
  cdef int dx, dy
  cdef float depth_value

  cdef np.ndarray[DTYPE_t, ndim=3] pointcloud = np.zeros((height, width, 3), dtype=np.float32)

  cdef float x,y, r2, f, ux, uy
  # cdef float ppx, ppy, c0, c1, c2, c3, c4
  # cdef float fx, fy

  # ppx = _ppx
  # ppy = _ppy
  # fx  = _fx
  # fy  = _fy
  # c0  = _coeffs[0]
  # c1  = _coeffs[1]
  # c2  = _coeffs[2]
  # c3  = _coeffs[3]
  # c4  = _coeffs[4]

  for dy in range(height):
      for dx in range(width):
          depth_value = depth_image[dy, dx] * depth_scale
          if depth_value == 0: continue

          x = (dx - ppx) / fx;
          y = (dy - ppy) / fy;
          r2  = x*x + y*y;
          f = 1 + c0*r2 + c1*r2*r2 + c4*r2*r2*r2;
          ux = x*f + 2*c2*x*y + c3*(r2 + 2*x*x);
          uy = y*f + 2*c3*x*y + c2*(r2 + 2*y*y);

          x = ux;
          y = uy;

          pointcloud[dy, dx, 0] = depth_value * x;
          pointcloud[dy, dx, 1] = depth_value * y;
          pointcloud[dy, dx, 2] = depth_value;

  return pointcloud

def sampler(int n, np.ndarray[float, ndim=2] points):
  """return n random rows of data (and also the other len(data)-n rows)"""
  cdef int length = len(points)
  cdef np.ndarray index = np.random.choice(length, n, replace=False)
  return points[index]

class PLANE():
  def __init__(self):
      self.center = None
      self.normal = None
  def fit(self, np.ndarray[float, ndim=2] points):
      normal = np.cross(np.subtract(points[1], points[0]), np.subtract(points[2] , points[0]))
      self.center = points[0]
      self.normal = np.divide(normal, np.linalg.norm(normal))
  def get_error(self, np.ndarray[float, ndim=2]points):
      vectors = np.subtract(points, self.center)
      distances = np.abs(np.dot(vectors, self.normal))
      return distances

def random_sample(int range_, int sample_size):
  cdef random_int
  idx = np.empty(sample_size, dtype=np.int)
  for i in range(sample_size):
      random = rand()
      idx[i]=int((random/RAND_MAX)*range_)
  return idx

def ransac(np.ndarray[float, ndim=2]points, int iterations=100, float threshold=3e-3, int min_required=100):
  cdef int i = 0
  cdef int length = len(points)
  cdef np.ndarray index
  cdef np.ndarray[np.uint8_t, ndim = 1,cast=True] best_inliers

  cdef np.ndarray[float, ndim=1] center
  cdef np.ndarray[float, ndim=1] normal

  cdef np.ndarray[np.uint8_t, ndim = 1,cast=True] inliers
  cdef int n_inliers

  while i < iterations:
      index = random_sample(length-1, 3)
      n_points = points[index]

      normal = np.cross(np.subtract(n_points[1], n_points[0]), np.subtract(n_points[2] , n_points[0]))
      center = n_points[0]
      normal = np.divide(normal, np.linalg.norm(normal))

      vectors = np.subtract(points, center)
      distances = np.abs(np.dot(vectors, normal))

      inliers = distances <= threshold
      n_inliers = np.sum(inliers)

      if n_inliers > min_required:
          # print(n_inliers)
          best_inliers = inliers
          min_required = n_inliers
      i+=1
  if best_inliers is None:
      raise ValueError("did not meet fit acceptance criteria")
  return best_inliers

def filter_by_ransac(np.ndarray[float, ndim=2]points, int iterations=100, float threshold=3e-3, int min_required=100):
  return np.reshape(np.invert(ransac(points.reshape(-1,3), iterations=iterations, threshold=threshold, min_required=min_required)), np.shape(points)[0:-1])
